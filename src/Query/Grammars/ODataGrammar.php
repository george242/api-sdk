<?php
namespace Jtl\Fulfillment\Api\Sdk\Query\Grammars;

use InvalidArgumentException;
use Jtl\Fulfillment\Api\Sdk\Query\Builder;

class ODataGrammar implements GrammarInterface
{
    protected const QUERY_PARAM = '$filter=';

    /**
     * @param Builder $query
     * @return string
     */
    public function compileWheres(Builder $query): string
    {
        // Each type of where clauses has its own compiler function which is responsible
        // for actually creating the where clauses SQL. This helps keep the code nice
        // and maintainable since each clause has a very small method that it uses.
        if ($query->wheres === null) {
            return '';
        }

        // If we actually have some where clauses, we will strip off the first boolean
        // operator, which is added by the query builders for convenience so we can
        // avoid checking for the first clauses in each of the compilers methods.
        if (count($sql = $this->internCompileWheresToArray($query)) > 0) {
            return $this->concatenateWhereClauses($query, $sql);
        }

        return '';
    }

    /**
     * @param Builder $query
     * @param array $data
     * @param int $index
     * @return array
     */
    public function compileWheresToArray(Builder $query, array &$data = [], int $index = 0): array
    {
        foreach ($query->wheres as $where) {
            if ($where['type'] === 'Nested') {
                $this->compileWheresToArray($where['query'], $data);
                continue;
            }

            if ($where['boolean'] === 'or') {
                $index++;
            }

            $data[$index][] = $this->whereBasic($query, $where);
        }

        return array_values(array_map(function ($ands) {
            return $this->removeLeadingBoolean(implode(',', $ands));
        }, $data));
    }

    /**
     * @param Builder $query
     * @return string
     */
    public function compileExpands(Builder $query): string
    {
        if ($query->expands === null || !is_array($query->expands) || count($query->expands) === 0) {
            return '';
        }

        return sprintf('$expand=%s', implode(',', $query->expands));
    }

    /**
     * @param Builder $query
     * @param array $data
     * @param int $index
     * @return array
     */
    public function compileExpandsToArray(Builder $query, array &$data = [], int $index = 0): array
    {
        foreach ($query->expands as $expand) {
            $data[$index][] = $expand;
        }
    }

    /**
     * Get an array of all the where clauses for the query.
     *
     * @param Builder $query
     * @return array
     */
    protected function internCompileWheresToArray(Builder $query): array
    {
        return array_map(function ($where) use ($query) {
            return sprintf('%s %s', $where['boolean'], $this->{"where{$where['type']}"}($query, $where));
        }, $query->wheres);
    }

    /**
     * Format the where clause statements into one string.
     *
     * @param Builder $query
     * @param array $sql
     * @return string
     */
    protected function concatenateWhereClauses(Builder $query, array $sql): string
    {
        return sprintf('%s%s', self::QUERY_PARAM, $this->removeLeadingBoolean(implode(' ', $sql)));
    }

    /**
     * Remove the leading boolean from a statement.
     *
     * @param string $value
     * @return string
     */
    protected function removeLeadingBoolean(string $value): string
    {
        return preg_replace('/and |or /i', '', $value, 1);
    }

    /**
     * Compile a basic where clause.
     *
     * @param Builder $query
     * @param array $where
     * @return string
     */
    protected function whereBasic(Builder $query, array $where): string
    {
        return sprintf('%s %s %s', $where['column'], $this->translateOperator($where['operator']), $this->escapeValue($where['value']));
    }

    /**
     * Compile a nested where clause.
     *
     * @param Builder $query
     * @param array $where
     * @return string
     */
    protected function whereNested(Builder $query, $where): string
    {
        return sprintf('(%s)', substr($this->compileWheres($where['query']), strlen(self::QUERY_PARAM)));
    }

    /**
     * Compile a contains where clause.
     *
     * @param Builder $query
     * @param $where
     * @return string
     */
    protected function whereContains(Builder $query, $where): string
    {
        return sprintf('contains(%s, \'%s\')', $where['column'], $where['value']);
    }

    /**
     * Compile a contains where clause.
     *
     * @param Builder $query
     * @param $where
     * @return string
     */
    protected function whereStartsWith(Builder $query, $where): string
    {
        return sprintf('startswith(%s, \'%s\')', $where['column'], $where['value']);
    }

    /**
     * Compile a contains where clause.
     *
     * @param Builder $query
     * @param $where
     * @return string
     */
    protected function whereEndsWith(Builder $query, $where): string
    {
        return sprintf('endswith(%s, \'%s\')', $where['column'], $where['value']);
    }

    /**
     * Compile a contains where clause.
     *
     * @param Builder $query
     * @param $where
     * @return string
     */
    protected function whereIn(Builder $query, $where): string
    {
        return sprintf('%s in (%s)', $where['column'], implode(',', array_map(function ($val) {
            return sprintf('%s', $this->escapeValue($val));
        }, $where['values'])));
    }

    /**
     * @param Builder $query
     * @param $where
     * @return string
     */
    protected function whereSub(Builder $query, $where): string
    {
        return sprintf('%s/any(x:x%s)', $where['child'], ($where['column'] ? '/' : '') . $this->whereBasic($query, $where));
    }

    /**
     * @param string $operator
     * @return string
     * @throws InvalidArgumentException
     */
    protected function translateOperator(string $operator): string
    {
        switch ($operator) {
            case Builder::OPERATOR_NOT_EQUAL:
                return 'ne';
                break;
            case Builder::OPERATOR_EQUAL:
                return 'eq';
                break;
            case Builder::OPERATOR_GREATER_EQUAL:
                return 'ge';
                break;
            case Builder::OPERATOR_LESS_EQUAL:
                return 'le';
                break;
            case Builder::OPERATOR_GREATER_THAN:
                return 'gt';
                break;
            case Builder::OPERATOR_LESS_THAN:
                return 'lt';
                break;
            default:
                throw new InvalidArgumentException('Unsupported operator');
        }
    }

    /**
     * @param $value
     * @return int|string
     */
    protected function escapeValue($value)
    {
        if (!is_string($value) && is_numeric($value)) {
            return $value + 0;
        }

        if (!is_string($value) && is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        return sprintf('\'%s\'', $value);
    }
}
