<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\OutboundShippingNotificationItem;

abstract class StockChange extends DataModel
{
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var StockChangeId|null
     */
    protected $stockChangeId;
    
    /**
     * @var float|null
     */
    protected $stockLevel;
    
    /**
     * @var float|null
     */
    protected $stockLevelReserved;
    
    /**
     * @var float|null
     */
    protected $stockLevelBlocked;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var float|null
     */
    protected $quantityReserved;
    
    /**
     * @var float|null
     */
    protected $quantityBlocked;
    
    /**
     * @var float|null
     */
    protected $quantityAnnounced;
    
    /**
     * @var float|null
     */
    protected $stockLevelAnnounced;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $batch;
    
    /**
     * @var BestBefore|null
     */
    protected $bestBefore;
    
    /**
     * @var string|null
     */
    protected $changeType;
    
    /**
     * @var string|null
     */
    protected $fulfillerStockChangeId;

    /**
     * @var StockReturnItem|null
     */
    protected $returnItem;
    
    /**
     * @var string|null
     */
    protected $merchantSku;

    /**
     * @var bool
     */
    protected $current = false;
    
    /**
     * @var OutboundShippingNotificationItem|null
     */
    protected $outboundShippingNotificationItem;
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return StockChange
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): StockChange
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return StockChangeId|null
     */
    public function getStockChangeId(): ?StockChangeId
    {
        return $this->stockChangeId;
    }
    
    /**
     * @param StockChangeId|null $stockChangeId
     * @return StockChange
     */
    public function setStockChangeId(?StockChangeId $stockChangeId): StockChange
    {
        $this->stockChangeId = $stockChangeId;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getStockLevel(): ?float
    {
        return $this->stockLevel;
    }
    
    /**
     * @param float|null $stockLevel
     * @return StockChange
     */
    public function setStockLevel(?float $stockLevel): StockChange
    {
        $this->stockLevel = $stockLevel;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getStockLevelReserved(): ?float
    {
        return $this->stockLevelReserved;
    }
    
    /**
     * @param float|null $stockLevelReserved
     * @return StockChange
     */
    public function setStockLevelReserved(?float $stockLevelReserved): StockChange
    {
        $this->stockLevelReserved = $stockLevelReserved;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getStockLevelBlocked(): ?float
    {
        return $this->stockLevelBlocked;
    }
    
    /**
     * @param float|null $stockLevelBlocked
     * @return StockChange
     */
    public function setStockLevelBlocked(?float $stockLevelBlocked): StockChange
    {
        $this->stockLevelBlocked = $stockLevelBlocked;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return StockChange
     */
    public function setQuantity(?float $quantity): StockChange
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityReserved(): ?float
    {
        return $this->quantityReserved;
    }
    
    /**
     * @param float|null $quantityReserved
     * @return StockChange
     */
    public function setQuantityReserved(?float $quantityReserved): StockChange
    {
        $this->quantityReserved = $quantityReserved;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityBlocked(): ?float
    {
        return $this->quantityBlocked;
    }
    
    /**
     * @param float|null $quantityBlocked
     * @return StockChange
     */
    public function setQuantityBlocked(?float $quantityBlocked): StockChange
    {
        $this->quantityBlocked = $quantityBlocked;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityAnnounced(): ?float
    {
        return $this->quantityAnnounced;
    }
    
    /**
     * @param float|null $quantityAnnounced
     * @return StockChange
     */
    public function setQuantityAnnounced(?float $quantityAnnounced): StockChange
    {
        $this->quantityAnnounced = $quantityAnnounced;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getStockLevelAnnounced(): ?float
    {
        return $this->stockLevelAnnounced;
    }
    
    /**
     * @param float|null $stockLevelAnnounced
     * @return StockChange
     */
    public function setStockLevelAnnounced(?float $stockLevelAnnounced): StockChange
    {
        $this->stockLevelAnnounced = $stockLevelAnnounced;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return StockChange
     */
    public function setNote(?string $note): StockChange
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }
    
    /**
     * @param string|null $batch
     * @return StockChange
     */
    public function setBatch(?string $batch): StockChange
    {
        $this->batch = $batch;
        
        return $this;
    }
    
    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }
    
    /**
     * @param BestBefore $bestBefore
     * @return StockChange
     */
    public function setBestBefore(BestBefore $bestBefore): StockChange
    {
        $this->bestBefore = $bestBefore;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getChangeType(): ?string
    {
        return $this->changeType;
    }
    
    /**
     * @param string|null $changeType
     * @return StockChange
     */
    public function setChangeType(?string $changeType): StockChange
    {
        $this->changeType = $changeType;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerStockChangeId(): ?string
    {
        return $this->fulfillerStockChangeId;
    }
    
    /**
     * @param string|null $fulfillerStockChangeId
     * @return StockChange
     */
    public function setFulfillerStockChangeId(?string $fulfillerStockChangeId): StockChange
    {
        $this->fulfillerStockChangeId = $fulfillerStockChangeId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return StockChange
     */
    public function setMerchantSku(?string $merchantSku): StockChange
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return OutboundShippingNotificationItem|null
     */
    public function getOutboundShippingNotificationItem(): ?OutboundShippingNotificationItem
    {
        return $this->outboundShippingNotificationItem;
    }
    
    /**
     * @param OutboundShippingNotificationItem $outboundShippingNotificationItem
     * @return StockChange
     */
    public function setOutboundShippingNotificationItem(
        OutboundShippingNotificationItem $outboundShippingNotificationItem
    ): StockChange {
        $this->outboundShippingNotificationItem = $outboundShippingNotificationItem;
        
        return $this;
    }

    /**
     * @return StockReturnItem|null
     */
    public function getReturnItem(): ?StockReturnItem
    {
        return $this->returnItem;
    }

    /**
     * @param StockReturnItem|null $returnItem
     * @return StockChange
     */
    public function setReturnItem(?StockReturnItem $returnItem): StockChange
    {
        $this->returnItem = $returnItem;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCurrent(): bool
    {
        return $this->current;
    }

    /**
     * @return bool
     */
    public function getCurrent(): bool
    {
        return $this->current;
    }

    /**
     * @param bool $current
     * @return StockChange
     */
    public function setCurrent(bool $current): StockChange
    {
        $this->current = $current;
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('stockChangeId', StockChangeId::class, null, true),
            new PropertyInfo('stockLevel', 'float', null),
            new PropertyInfo('stockLevelReserved', 'float', null),
            new PropertyInfo('stockLevelBlocked', 'float', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('quantityReserved', 'float', null),
            new PropertyInfo('quantityBlocked', 'float', null),
            new PropertyInfo('quantityAnnounced', 'float', null),
            new PropertyInfo('stockLevelAnnounced', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('changeType', 'string', null),
            new PropertyInfo('fulfillerStockChangeId', 'string', null),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('current', 'bool', false),
            new PropertyInfo('returnItem', StockReturnItem::class, null, true),
            new PropertyInfo('outboundShippingNotificationItem', OutboundShippingNotificationItem::class, null, true)
        ]);
    }
}
