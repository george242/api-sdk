<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;

/**
 * Class StockLevelDetail
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Stock
 */
class StockLevelDetail extends DataModel
{
    /**
     * @var string|null
     */
    protected $batch;

    /**
     * @var BestBefore|null
     */
    protected $bestBefore;

    /**
     * @var float|null
     */
    protected $stockLevel;

    /**
     * @var float|null
     */
    protected $stockLevelBlocked;

    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }

    /**
     * @param string|null $batch
     * @return StockLevelDetail
     */
    public function setBatch(?string $batch): StockLevelDetail
    {
        $this->batch = $batch;

        return $this;
    }

    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }

    /**
     * @param BestBefore $bestBefore
     * @return StockLevelDetail
     */
    public function setBestBefore(BestBefore $bestBefore): StockLevelDetail
    {
        $this->bestBefore = $bestBefore;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevel(): ?float
    {
        return $this->stockLevel;
    }

    /**
     * @param float|null $stockLevel
     * @return StockLevelDetail
     */
    public function setStockLevel(?float $stockLevel): StockLevelDetail
    {
        $this->stockLevel = $stockLevel;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevelBlocked(): ?float
    {
        return $this->stockLevelBlocked;
    }

    /**
     * @param float|null $stockLevelBlocked
     * @return StockLevelDetail
     */
    public function setStockLevelBlocked(?float $stockLevelBlocked): StockLevelDetail
    {
        $this->stockLevelBlocked = $stockLevelBlocked;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('stockLevel', 'float', null),
            new PropertyInfo('stockLevelBlocked', 'float', null)
        ]);
    }
}
