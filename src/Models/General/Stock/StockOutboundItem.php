<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class StockOutboundItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $outboundItemId;
    
    /**
     * @return string|null
     */
    public function getOutboundItemId(): ?string
    {
        return $this->outboundItemId;
    }
    
    /**
     * @param string|null $outboundItemId
     * @return StockOutboundItem
     */
    public function setOutboundItemId(?string $outboundItemId): StockOutboundItem
    {
        $this->outboundItemId = $outboundItemId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('outboundItemId', 'string', null)
        ]);
    }
}
