<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class StockLevel
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Stock
 */
class StockLevel extends DataModel
{
    /**
     * @var string|null
     */
    protected $jfsku;

    /**
     * @var float|null
     */
    protected $stockLevel;

    /**
     * @var float|null
     */
    protected $stockLevelAnnounced;

    /**
     * @var float|null
     */
    protected $stockLevelReserved;

    /**
     * @var float|null
     */
    protected $stockLevelBlocked;

    /**
     * @var StockLevelDetail[]
     */
    protected $stockLevelDetails = [];

    /**
     * @var StockAnnouncedDetail[]
     */
    protected $stockAnnouncedDetails = [];

    /**
     * @var StockReservedDetail[]
     */
    protected $stockReservedDetails = [];

    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }

    /**
     * @param string|null $jfsku
     * @return StockLevel
     */
    public function setJfsku(?string $jfsku): StockLevel
    {
        $this->jfsku = $jfsku;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevel(): ?float
    {
        return $this->stockLevel;
    }

    /**
     * @param float|null $stockLevel
     * @return StockLevel
     */
    public function setStockLevel(?float $stockLevel): StockLevel
    {
        $this->stockLevel = $stockLevel;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevelAnnounced(): ?float
    {
        return $this->stockLevelAnnounced;
    }

    /**
     * @param float|null $stockLevelAnnounced
     * @return StockLevel
     */
    public function setStockLevelAnnounced(?float $stockLevelAnnounced): StockLevel
    {
        $this->stockLevelAnnounced = $stockLevelAnnounced;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevelReserved(): ?float
    {
        return $this->stockLevelReserved;
    }

    /**
     * @param float|null $stockLevelReserved
     * @return StockLevel
     */
    public function setStockLevelReserved(?float $stockLevelReserved): StockLevel
    {
        $this->stockLevelReserved = $stockLevelReserved;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getStockLevelBlocked(): ?float
    {
        return $this->stockLevelBlocked;
    }

    /**
     * @param float|null $stockLevelBlocked
     * @return StockLevel
     */
    public function setStockLevelBlocked(?float $stockLevelBlocked): StockLevel
    {
        $this->stockLevelBlocked = $stockLevelBlocked;

        return $this;
    }

    /**
     * @return StockLevelDetail[]
     */
    public function getStockLevelDetails(): array
    {
        return $this->stockLevelDetails;
    }

    /**
     * @param StockLevelDetail[] $stockLevelDetails
     * @return StockLevel
     */
    public function setStockLevelDetails(array $stockLevelDetails): StockLevel
    {
        $this->stockLevelDetails = $stockLevelDetails;

        return $this;
    }

    /**
     * @param StockLevelDetail $stockLevelDetail
     * @param string|null $key
     * @return StockLevel
     */
    public function addStockLevelDetail(StockLevelDetail $stockLevelDetail, string $key = null): StockLevel
    {
        if ($key === null) {
            $this->stockLevelDetails[] = $stockLevelDetail;
        } else {
            $this->stockLevelDetails[$key] = $stockLevelDetail;
        }

        return $this;
    }

    /**
     * @return StockAnnouncedDetail[]
     */
    public function getStockAnnouncedDetails(): array
    {
        return $this->stockAnnouncedDetails;
    }

    /**
     * @param StockAnnouncedDetail[] $stockAnnouncedDetails
     * @return StockLevel
     */
    public function setStockAnnouncedDetails(array $stockAnnouncedDetails): StockLevel
    {
        $this->stockAnnouncedDetails = $stockAnnouncedDetails;

        return $this;
    }

    /**
     * @param StockAnnouncedDetail $stockAnnouncedDetail
     * @param string|null $key
     * @return StockLevel
     */
    public function addStockAnnouncedDetail(StockAnnouncedDetail $stockAnnouncedDetail, string $key = null): StockLevel
    {
        if ($key === null) {
            $this->stockAnnouncedDetails[] = $stockAnnouncedDetail;
        } else {
            $this->stockAnnouncedDetails[$key] = $stockAnnouncedDetail;
        }

        return $this;
    }

    /**
     * @return StockReservedDetail[]
     */
    public function getStockReservedDetails(): array
    {
        return $this->stockReservedDetails;
    }

    /**
     * @param StockReservedDetail[] $stockReservedDetails
     * @return StockLevel
     */
    public function setStockReservedDetails(array $stockReservedDetails): StockLevel
    {
        $this->stockReservedDetails = $stockReservedDetails;

        return $this;
    }

    /**
     * @param StockReservedDetail $stockReservedDetail
     * @param string|null $key
     * @return StockLevel
     */
    public function addStockReservedDetail(StockReservedDetail $stockReservedDetail, string $key = null): StockLevel
    {
        if ($key === null) {
            $this->stockReservedDetails[] = $stockReservedDetail;
        } else {
            $this->stockReservedDetails[$key] = $stockReservedDetail;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('stockLevel', 'float', null),
            new PropertyInfo('stockLevelAnnounced', 'float', null),
            new PropertyInfo('stockLevelReserved', 'float', null),
            new PropertyInfo('stockLevelBlocked', 'float', null),
            new PropertyInfo('stockLevelDetails', StockLevelDetail::class, [], true, true),
            new PropertyInfo('stockAnnouncedDetails', StockAnnouncedDetail::class, [], true, true),
            new PropertyInfo('stockReservedDetails', StockReservedDetail::class, [], true, true)
        ]);
    }
}
