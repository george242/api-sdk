<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

class ChangeValueInt extends DataModel
{
    /**
     * @var int|null
     */
    protected $value;

    /**
     * @var int|null
     */
    protected $previousValue;

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int|null $value
     * @return ChangeValueInt
     */
    public function setValue(?int $value): ChangeValueInt
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPreviousValue(): ?int
    {
        return $this->previousValue;
    }

    /**
     * @param int|null $previousValue
     * @return ChangeValueInt
     */
    public function setPreviousValue(?int $previousValue): ChangeValueInt
    {
        $this->previousValue = $previousValue;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('value', 'int', null),
            new PropertyInfo('previousValue', 'int', null),
        ]);
    }
}
