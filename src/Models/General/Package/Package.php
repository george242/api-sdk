<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Package;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Package
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Package
 */
class Package extends DataModel
{
    /**
     * @var string|null
     */
    protected $carrierCode;
    
    /**
     * @var string|null
     */
    protected $carrierName;
    
    /**
     * @var DateTime|null
     */
    protected $estimatedDeliveryDate;
    
    /**
     * @var string|null
     */
    protected $freightOption;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $trackingUrl;
    
    /**
     * @var PackageIdentifier[]
     */
    protected $identifier = [];
    
    /**
     * @var int|null
     */
    protected $shipmentCount;
    
    /**
     * @var DateTime|null
     */
    protected $shippingDate;
    
    /**
     * @var string|null
     */
    protected $shippingMethodId;
    
    /**
     * @return string|null
     */
    public function getCarrierCode(): ?string
    {
        return $this->carrierCode;
    }
    
    /**
     * @param string|null $carrierCode
     * @return Package
     */
    public function setCarrierCode(?string $carrierCode): Package
    {
        $this->carrierCode = $carrierCode;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }
    
    /**
     * @param string|null $carrierName
     * @return Package
     */
    public function setCarrierName(?string $carrierName): Package
    {
        $this->carrierName = $carrierName;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getEstimatedDeliveryDate(): ?DateTime
    {
        return $this->estimatedDeliveryDate;
    }
    
    /**
     * @param DateTime|string|null $estimatedDeliveryDate
     * @return Package
     * @throws Exception
     */
    public function setEstimatedDeliveryDate($estimatedDeliveryDate): Package
    {
        if ($estimatedDeliveryDate === null) {
            return $this;
        }
    
        if (is_string($estimatedDeliveryDate)) {
            $estimatedDeliveryDate = (new DateTime($estimatedDeliveryDate))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($estimatedDeliveryDate);
        $this->estimatedDeliveryDate = $estimatedDeliveryDate;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFreightOption(): ?string
    {
        return $this->freightOption;
    }
    
    /**
     * @param string|null $freightOption
     * @return Package
     */
    public function setFreightOption(?string $freightOption): Package
    {
        $this->freightOption = $freightOption;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return Package
     */
    public function setNote(?string $note): Package
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTrackingUrl(): ?string
    {
        return $this->trackingUrl;
    }
    
    /**
     * @param string|null $trackingUrl
     * @return Package
     */
    public function setTrackingUrl(?string $trackingUrl): Package
    {
        $this->trackingUrl = $trackingUrl;
        
        return $this;
    }
    
    /**
     * @return PackageIdentifier[]
     */
    public function getIdentifier(): array
    {
        return $this->identifier;
    }
    
    /**
     * @param PackageIdentifier[] $identifier
     * @return Package
     */
    public function setIdentifier(array $identifier): Package
    {
        $this->identifier = $identifier;
        
        return $this;
    }
    
    /**
     * @param PackageIdentifier $identifier
     * @param string|null $key
     * @return Package
     */
    public function addIdentifier(PackageIdentifier $identifier, string $key = null): Package
    {
        if ($key === null) {
            $this->identifier[] = $identifier;
        } else {
            $this->identifier[$key] = $identifier;
        }
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getShipmentCount(): ?int
    {
        return $this->shipmentCount;
    }
    
    /**
     * @param int|null $shipmentCount
     * @return Package
     */
    public function setShipmentCount(?int $shipmentCount): Package
    {
        $this->shipmentCount = $shipmentCount;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getShippingDate(): ?DateTime
    {
        return $this->shippingDate;
    }
    
    /**
     * @param DateTime|string|null $shippingDate
     * @return Package
     * @throws Exception
     */
    public function setShippingDate($shippingDate): Package
    {
        if ($shippingDate === null) {
            return $this;
        }
    
        if (is_string($shippingDate)) {
            $shippingDate = (new DateTime($shippingDate))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($shippingDate);
        $this->shippingDate = $shippingDate;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getShippingMethodId(): ?string
    {
        return $this->shippingMethodId;
    }
    
    /**
     * @param string|null $shippingMethodId
     * @return Package
     */
    public function setShippingMethodId(?string $shippingMethodId): Package
    {
        $this->shippingMethodId = $shippingMethodId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('carrierCode', 'string', null),
            new PropertyInfo('carrierName', 'string', null),
            new PropertyInfo('estimatedDeliveryDate', DateTime::class, null),
            new PropertyInfo('freightOption', 'string', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('trackingUrl', 'string', null),
            new PropertyInfo('identifier', PackageIdentifier::class, [], true, true),
            new PropertyInfo('shipmentCount', 'int', null),
            new PropertyInfo('shippingDate', DateTime::class, null),
            new PropertyInfo('shippingMethodId', 'string', null)
        ]);
    }
}
