<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Attachment
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Attachment extends DataModel
{
    public const TYPE_INVOICE = 'Invoice';
    public const TYPE_DELIVERY_NOTE = 'DeliveryNote';
    public const TYPE_SHIPPING_LABEL = 'ShippingLabel';
    public const TYPE_CUSTOM = 'Custom';
    
    /**
     * @var string|null
     */
    protected $url;
    
    /**
     * @var string|null - Base64 Coded String
     */
    protected $data;
    
    /**
     * @var int|null
     */
    protected $size;
    
    /**
     * @var string|null
     */
    protected $mimeType;
    
    /**
     * @var string|null
     */
    protected $merchantDocumentId;
    
    /**
     * @var string|null
     */
    protected $documentType;
    
    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
    
    /**
     * @param string|null $url
     * @return Attachment
     */
    public function setUrl(?string $url): Attachment
    {
        $this->url = $url;
        
        return $this;
    }
    
    /**
     * @return string|null - Base64 Coded String
     */
    public function getData(): ?string
    {
        return $this->data;
    }
    
    /**
     * @param string|null $data - Base64 Coded String
     * @return Attachment
     */
    public function setData(?string $data): Attachment
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }
    
    /**
     * @param int|null $size
     * @return Attachment
     */
    public function setSize(?int $size): Attachment
    {
        $this->size = $size;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }
    
    /**
     * @param string|null $mimeType
     * @return Attachment
     */
    public function setMimeType(?string $mimeType): Attachment
    {
        $this->mimeType = $mimeType;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantDocumentId(): ?string
    {
        return $this->merchantDocumentId;
    }
    
    /**
     * @param string|null $merchantDocumentId
     * @return Attachment
     */
    public function setMerchantDocumentId(?string $merchantDocumentId): Attachment
    {
        $this->merchantDocumentId = $merchantDocumentId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getDocumentType(): ?string
    {
        return $this->documentType;
    }
    
    /**
     * @param string|null $documentType
     * @return Attachment
     */
    public function setDocumentType(?string $documentType): Attachment
    {
        $this->documentType = $documentType;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('url', 'string', null),
            new PropertyInfo('data', 'string', null),
            new PropertyInfo('size', 'int', null),
            new PropertyInfo('mimeType', 'string', null),
            new PropertyInfo('merchantDocumentId', 'string', null),
            new PropertyInfo('documentType', 'string', null)
        ]);
    }
}
