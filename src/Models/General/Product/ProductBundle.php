<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class ProductBundle extends DataModel
{
    /**
     * @var string|null
     */
    protected $ean;
    
    /**
     * @var string|null
     */
    protected $upc;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @return string|null
     */
    public function getEan(): ?string
    {
        return $this->ean;
    }
    
    /**
     * @param string|null $ean
     * @return ProductBundle
     */
    public function setEan(?string $ean): ProductBundle
    {
        $this->ean = $ean;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUpc(): ?string
    {
        return $this->upc;
    }
    
    /**
     * @param string|null $upc
     * @return ProductBundle
     */
    public function setUpc(?string $upc): ProductBundle
    {
        $this->upc = $upc;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return ProductBundle
     */
    public function setName(?string $name): ProductBundle
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return ProductBundle
     */
    public function setQuantity(?float $quantity): ProductBundle
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('ean', 'string', null),
            new PropertyInfo('upc', 'string', null),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('quantity', 'float', null),
        ]);
    }
}
