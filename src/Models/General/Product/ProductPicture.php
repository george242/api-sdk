<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Resources\General\ProductResource;
use Throwable;

/**
 * Class ProductPicture
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductPicture extends DataModel
{
    /**
     * @var string|null
     */
    protected $publicUrl;
    
    /**
     * @var string|null
     */
    protected $data;
    
    /**
     * @var string|null
     */
    protected $url;
    
    /**
     * @var string|null
     */
    protected $hash;
    
    /**
     * @var int|null
     */
    protected $size;
    
    /**
     * @var string|null
     */
    protected $mimeType;
    
    /**
     * @var int|null
     */
    protected $number;
    
    /**
     * @return string|null
     */
    public function getPublicUrl(): ?string
    {
        return $this->publicUrl;
    }
    
    /**
     * @param string $publicUrl
     * @return ProductPicture
     */
    public function setPublicUrl(?string $publicUrl): ProductPicture
    {
        $this->publicUrl = $publicUrl;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return $this->data;
    }
    
    /**
     * @param string|null $data
     * @return ProductPicture
     */
    public function setData(?string $data): ProductPicture
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }
    
    /**
     * @param string|null $url
     * @return ProductPicture
     */
    public function setUrl(?string $url): ProductPicture
    {
        $this->url = $url;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }
    
    /**
     * @param string $hash
     * @return ProductPicture
     */
    public function setHash(?string $hash): ProductPicture
    {
        $this->hash = $hash;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }
    
    /**
     * @param int|null $size
     * @return ProductPicture
     */
    public function setSize(?int $size): ProductPicture
    {
        $this->size = $size;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }
    
    /**
     * @param string $mimeType
     * @return ProductPicture
     */
    public function setMimeType(?string $mimeType): ProductPicture
    {
        $this->mimeType = $mimeType;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }
    
    /**
     * @param int|null $number
     * @return ProductPicture
     */
    public function setNumber(?int $number): ProductPicture
    {
        $this->number = $number;
        
        return $this;
    }
    
    /**
     * @param ProductResource $resource
     * @return bool
     * @throws Throwable
     */
    public function fillData(ProductResource $resource): bool
    {
        if (empty($this->getPublicUrl())) {
            return false;
        }
        
        $this->setData($resource->loadDataFromUrl($this->getPublicUrl()));
        
        return true;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('publicUrl', 'string', null),
            new PropertyInfo('data', 'string', null),
            new PropertyInfo('url', 'string', null),
            new PropertyInfo('hash', 'string', null),
            new PropertyInfo('size', 'int', null),
            new PropertyInfo('mimeType', 'string', null),
            new PropertyInfo('number', 'int', null),
        ]);
    }
}
