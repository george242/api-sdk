<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ProductIdentifier
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductIdentifier extends DataModel
{
    /**
     * @var ProductMpn|null
     */
    protected $mpn;
    
    /**
     * @var string|null
     */
    protected $ean;
    
    /**
     * @var string|null
     */
    protected $isbn;
    
    /**
     * @var string|null
     */
    protected $upc;
    
    /**
     * @var string|null
     */
    protected $asin;
    
    /**
     * @return ProductMpn|null
     */
    public function getMpn(): ?ProductMpn
    {
        return $this->mpn;
    }
    
    /**
     * @param ProductMpn $mpn
     * @return ProductIdentifier
     */
    public function setMpn(ProductMpn $mpn): ProductIdentifier
    {
        $this->mpn = $mpn;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getEan(): ?string
    {
        return $this->ean;
    }
    
    /**
     * @param string|null $ean
     * @return ProductIdentifier
     */
    public function setEan(?string $ean): ProductIdentifier
    {
        $this->ean = $ean;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getIsbn(): ?string
    {
        return $this->isbn;
    }
    
    /**
     * @param string|null $isbn
     * @return ProductIdentifier
     */
    public function setIsbn(?string $isbn): ProductIdentifier
    {
        $this->isbn = $isbn;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUpc(): ?string
    {
        return $this->upc;
    }
    
    /**
     * @param string|null $upc
     * @return ProductIdentifier
     */
    public function setUpc(?string $upc): ProductIdentifier
    {
        $this->upc = $upc;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getAsin(): ?string
    {
        return $this->asin;
    }
    
    /**
     * @param string|null $asin
     * @return ProductIdentifier
     */
    public function setAsin(?string $asin): ProductIdentifier
    {
        $this->asin = $asin;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('mpn', ProductMpn::class, null, true),
            new PropertyInfo('ean', 'string', null),
            new PropertyInfo('isbn', 'string', null),
            new PropertyInfo('upc', 'string', null),
            new PropertyInfo('asin', 'string', null)
        ]);
    }
}
