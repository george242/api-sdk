<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ProductMpn
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductMpn extends DataModel
{
    /**
     * @var string|null
     */
    protected $manufacturer;
    
    /**
     * @var string|null
     */
    protected $partNumber;
    
    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }
    
    /**
     * @param string|null $manufacturer
     * @return ProductMpn
     */
    public function setManufacturer(?string $manufacturer): ProductMpn
    {
        $this->manufacturer = $manufacturer;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPartNumber(): ?string
    {
        return $this->partNumber;
    }
    
    /**
     * @param string|null $partNumber
     * @return ProductMpn
     */
    public function setPartNumber(?string $partNumber): ProductMpn
    {
        $this->partNumber = $partNumber;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('manufacturer', 'string', null),
            new PropertyInfo('partNumber', 'string', null)
        ]);
    }
}
