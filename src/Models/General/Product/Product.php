<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attribute;
use Jtl\Fulfillment\Api\Sdk\Models\General\Dimension;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Product
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product
 */
class Product extends DataModel
{
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var ProductPicture[]
     */
    protected $pictures = [];
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var string|null
     */
    protected $merchantSku;
    
    /**
     * @var string|null
     */
    protected $manufacturer;
    
    /**
     * @var string|null
     */
    protected $productGroup;
    
    /**
     * @var string|null - ISO 3166 Alpha 2
     */
    protected $originCountry;
    
    /**
     * @var float
     */
    protected $weight;
    
    /**
     * @var float
     */
    protected $netWeight;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var ProductStockLevel|null
     */
    protected $stock;
    
    /**
     * @var ProductIdentifier|null
     */
    protected $identifier;
    
    /**
     * @var ProductSpecification|null
     */
    protected $specifications;
    
    /**
     * @var Dimension|null
     */
    protected $dimensions;
    
    /**
     * @var Attribute[]
     */
    protected $attributes = [];
    
    /**
     * @var ProductPrice|null
     */
    protected $netRetailPrice;
    
    /**
     * @var ProductBundle[]
     */
    protected $bundles = [];
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Product
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Product
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return Product
     */
    public function setJfsku(?string $jfsku): Product
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return ProductPicture[]
     */
    public function getPictures(): array
    {
        return $this->pictures;
    }
    
    /**
     * @param ProductPicture[] $pictures
     * @return Product
     */
    public function setPictures(array $pictures): Product
    {
        $this->pictures = $pictures;
        
        return $this;
    }
    
    /**
     * @param ProductPicture $picture
     * @param string|null $key
     * @return Product
     */
    public function addPicture(ProductPicture $picture, ?string $key = null): Product
    {
        if ($key === null) {
            $this->pictures[] = $picture;
        } else {
            $this->pictures[$key] = $picture;
        }
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return Product
     */
    public function setName(?string $name): Product
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return Product
     */
    public function setMerchantSku(?string $merchantSku): Product
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }
    
    /**
     * @param string|null $manufacturer
     * @return Product
     */
    public function setManufacturer(?string $manufacturer): Product
    {
        $this->manufacturer = $manufacturer;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getProductGroup(): ?string
    {
        return $this->productGroup;
    }
    
    /**
     * @param string|null $productGroup
     * @return Product
     */
    public function setProductGroup(?string $productGroup): Product
    {
        $this->productGroup = $productGroup;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getOriginCountry(): ?string
    {
        return $this->originCountry;
    }
    
    /**
     * @param string|null $originCountry
     * @return Product
     */
    public function setOriginCountry(?string $originCountry): Product
    {
        $this->originCountry = $originCountry;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }
    
    /**
     * @param float|null $weight
     * @return Product
     */
    public function setWeight(?float $weight): Product
    {
        $this->weight = $weight;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getNetWeight(): ?float
    {
        return $this->netWeight;
    }
    
    /**
     * @param float|null $netWeight
     * @return Product
     */
    public function setNetWeight(?float $netWeight): Product
    {
        $this->netWeight = $netWeight;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return Product
     */
    public function setNote(?string $note): Product
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return ProductStockLevel|null
     */
    public function getStock(): ?ProductStockLevel
    {
        return $this->stock;
    }
    
    /**
     * @param ProductStockLevel|null $stock
     * @return Product
     */
    public function setStock(?ProductStockLevel $stock): Product
    {
        $this->stock = $stock;
        
        return $this;
    }
    
    /**
     * @return ProductIdentifier|null
     */
    public function getIdentifier(): ?ProductIdentifier
    {
        return $this->identifier;
    }
    
    /**
     * @param ProductIdentifier $identifier
     * @return Product
     */
    public function setIdentifier(ProductIdentifier $identifier): Product
    {
        $this->identifier = $identifier;
        
        return $this;
    }
    
    /**
     * @return ProductSpecification|null
     */
    public function getSpecifications(): ?ProductSpecification
    {
        return $this->specifications;
    }
    
    /**
     * @param ProductSpecification $specifications
     * @return Product
     */
    public function setSpecifications(ProductSpecification $specifications): Product
    {
        $this->specifications = $specifications;
        
        return $this;
    }
    
    /**
     * @return Dimension|null
     */
    public function getDimensions(): ?Dimension
    {
        return $this->dimensions;
    }
    
    /**
     * @param Dimension $dimensions
     * @return Product
     */
    public function setDimensions(Dimension $dimensions): Product
    {
        $this->dimensions = $dimensions;
        
        return $this;
    }
    
    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
    
    /**
     * @param Attribute[] $attributes
     * @return Product
     */
    public function setAttributes(array $attributes): Product
    {
        $this->attributes = $attributes;
        
        return $this;
    }
    
    /**
     * @param Attribute $attribute
     * @param string|null $key
     * @return Product
     */
    public function addAttribute(Attribute $attribute, ?string $key = null): Product
    {
        if ($key === null) {
            $this->attributes[] = $attribute;
        } else {
            $this->attributes[$key] = $attribute;
        }
        
        return $this;
    }
    
    /**
     * @return ProductPrice|null
     */
    public function getNetRetailPrice(): ?ProductPrice
    {
        return $this->netRetailPrice;
    }
    
    /**
     * @param ProductPrice $netRetailPrice
     * @return Product
     */
    public function setNetRetailPrice(ProductPrice $netRetailPrice): Product
    {
        $this->netRetailPrice = $netRetailPrice;
        
        return $this;
    }
    
    /**
     * @return ProductBundle[]
     */
    public function getBundles(): array
    {
        return $this->bundles;
    }
    
    /**
     * @param ProductBundle[] $bundles
     * @return Product
     */
    public function setBundles(array $bundles): Product
    {
        $this->bundles = $bundles;
        
        return $this;
    }
    
    /**
     * @param ProductBundle $bundle
     * @param string|null $key
     * @return Product
     */
    public function addBundle(ProductBundle $bundle, ?string $key = null): Product
    {
        if ($key === null) {
            $this->bundles[] = $bundle;
        } else {
            $this->bundles[$key] = $bundle;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('pictures', ProductPicture::class, [], true, true),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('manufacturer', 'string', null),
            new PropertyInfo('productGroup', 'string', null),
            new PropertyInfo('originCountry', 'string', null),
            new PropertyInfo('weight', 'float', null),
            new PropertyInfo('netWeight', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('stock', ProductStockLevel::class, null, true),
            new PropertyInfo('identifier', ProductIdentifier::class, null, true),
            new PropertyInfo('specifications', ProductSpecification::class, null, true),
            new PropertyInfo('dimensions', Dimension::class, null, true),
            new PropertyInfo('attributes', Attribute::class, [], true, true),
            new PropertyInfo('netRetailPrice', ProductPrice::class, null, true),
            new PropertyInfo('bundles', ProductBundle::class, [], true, true),
        ]);
    }
}
