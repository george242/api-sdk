<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class CarrierCode
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class CarrierCode extends DataModel
{
    /**
     * @var string|null
     */
    protected $code;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }
    
    /**
     * @param string|null $code
     * @return CarrierCode
     */
    public function setCode(?string $code): CarrierCode
    {
        $this->code = $code;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return CarrierCode
     */
    public function setName(?string $name): CarrierCode
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('code', 'string', null),
            new PropertyInfo('name', 'string', null)
        ]);
    }
}
