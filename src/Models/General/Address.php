<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Address
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Address extends DataModel
{
    public const MR = 'm';
    public const MRS = 'w';
    
    /**
     * @var string|null
     */
    protected $salutation;
    
    /**
     * @var string|null
     */
    protected $firstName;
    
    /**
     * @var string|null
     */
    protected $lastName;
    
    /**
     * @var string|null
     */
    protected $company;
    
    /**
     * @var string|null
     */
    protected $street;
    
    /**
     * @var string|null
     */
    protected $city;
    
    /**
     * @var string|null
     */
    protected $zip;
    
    /**
     * @var string|null
     */
    protected $country;
    
    /**
     * @var string|null
     */
    protected $email;
    
    /**
     * @var string|null
     */
    protected $phone;
    
    /**
     * @var string|null
     */
    protected $extraLine;
    
    /**
     * @var string|null
     */
    protected $extraAddressLine;
    
    /**
     * @var string|null
     */
    protected $state;
    
    /**
     * @var string|null
     */
    protected $mobile;
    
    /**
     * @var string|null
     */
    protected $fax;
    
    /**
     * @return string|null
     */
    public function getSalutation(): ?string
    {
        // Fix nasty stuff
        $this->fixApiSalutation();
        
        return $this->salutation;
    }
    
    /**
     * @param string|null $salutation
     * @return Address
     */
    public function setSalutation(?string $salutation): Address
    {
        $this->salutation = $salutation;
    
        // Fix nasty stuff
        $this->fixApiSalutation();
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }
    
    /**
     * @param string|null $firstName
     * @return Address
     */
    public function setFirstName(?string $firstName): Address
    {
        $this->firstName = $firstName;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
    
    /**
     * @param string|null $lastName
     * @return Address
     */
    public function setLastName(?string $lastName): Address
    {
        $this->lastName = $lastName;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return $this->company;
    }
    
    /**
     * @param string|null $company
     * @return Address
     */
    public function setCompany(?string $company): Address
    {
        $this->company = $company;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }
    
    /**
     * @param string|null $street
     * @return Address
     */
    public function setStreet(?string $street): Address
    {
        $this->street = $street;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }
    
    /**
     * @param string|null $city
     * @return Address
     */
    public function setCity(?string $city): Address
    {
        $this->city = $city;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }
    
    /**
     * @param string|null $zip
     * @return Address
     */
    public function setZip(?string $zip): Address
    {
        $this->zip = $zip;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }
    
    /**
     * @param string|null $country
     * @return Address
     */
    public function setCountry(?string $country): Address
    {
        $this->country = $country;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }
    
    /**
     * @param string|null $email
     * @return Address
     */
    public function setEmail(?string $email): Address
    {
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }
    
    /**
     * @param string|null $phone
     * @return Address
     */
    public function setPhone(?string $phone): Address
    {
        $this->phone = $phone;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExtraLine(): ?string
    {
        return $this->extraLine;
    }
    
    /**
     * @param string|null $extraLine
     * @return Address
     */
    public function setExtraLine(?string $extraLine): Address
    {
        $this->extraLine = $extraLine;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExtraAddressLine(): ?string
    {
        return $this->extraAddressLine;
    }
    
    /**
     * @param string|null $extraAddressLine
     * @return Address
     */
    public function setExtraAddressLine(?string $extraAddressLine): Address
    {
        $this->extraAddressLine = $extraAddressLine;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    
    /**
     * @param string|null $state
     * @return Address
     */
    public function setState(?string $state): Address
    {
        $this->state = $state;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMobile(): ?string
    {
        return $this->mobile;
    }
    
    /**
     * @param string|null $mobile
     * @return Address
     */
    public function setMobile(?string $mobile): Address
    {
        $this->mobile = $mobile;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }
    
    /**
     * @param string|null $fax
     * @return Address
     */
    public function setFax(?string $fax): Address
    {
        $this->fax = $fax;
        
        return $this;
    }
    
    public function fixApiSalutation(): void
    {
        // Fix nasty stuff
        if ($this->salutation === 'Herr' || $this->salutation === 'Mr') {
            $this->salutation = self::MR;
        }
    
        if ($this->salutation === 'Frau' || $this->salutation === 'Mrs') {
            $this->salutation = self::MRS;
        }
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('salutation', 'string', null),
            new PropertyInfo('firstName', 'string', null),
            new PropertyInfo('lastName', 'string', null),
            new PropertyInfo('company', 'string', null),
            new PropertyInfo('street', 'string', null),
            new PropertyInfo('city', 'string', null),
            new PropertyInfo('zip', 'string', null),
            new PropertyInfo('country', 'string', null),
            new PropertyInfo('email', 'string', null),
            new PropertyInfo('phone', 'string', null),
            new PropertyInfo('extraLine', 'string', null),
            new PropertyInfo('extraAddressLine', 'string', null),
            new PropertyInfo('state', 'string', null),
            new PropertyInfo('mobile', 'string', null),
            new PropertyInfo('fax', 'string', null)
        ]);
    }
}
