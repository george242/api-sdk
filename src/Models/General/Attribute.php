<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Attribute
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Attribute extends DataModel
{
    public const TYPE_STRING = 'String';
    public const TYPE_INTEGER = 'Integer';
    public const TYPE_DECIMAL = 'Decimal';
    public const TYPE_BOOLEAN = 'Boolean';
    public const TYPE_DATE = 'Date';
    
    /**
     * @var string|null
     */
    protected $key;
    
    /**
     * @var string|null
     */
    protected $value;
    
    /**
     * @var string|null
     */
    protected $attributeType = self::TYPE_STRING;
    
    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }
    
    /**
     * @param string|null $key
     * @return Attribute
     */
    public function setKey(?string $key): Attribute
    {
        $this->key = $key;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }
    
    /**
     * @param string|null $value
     * @return Attribute
     */
    public function setValue(?string $value): Attribute
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getAttributeType(): ?string
    {
        return $this->attributeType;
    }
    
    /**
     * @param string|null $attributeType
     * @return Attribute
     */
    public function setAttributeType(?string $attributeType): Attribute
    {
        $this->attributeType = $attributeType;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('key', 'string', null),
            new PropertyInfo('value', 'string', null),
            new PropertyInfo('attributeType', 'string', self::TYPE_STRING)
        ]);
    }
}
