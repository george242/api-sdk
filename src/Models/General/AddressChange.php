<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class AddressChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class AddressChange extends DataModel
{
    /**
     * @var ChangeValue|null
     */
    protected $salutation;

    /**
     * @var ChangeValue|null
     */
    protected $firstName;

    /**
     * @var ChangeValue|null
     */
    protected $lastName;

    /**
     * @var ChangeValue|null
     */
    protected $company;

    /**
     * @var ChangeValue|null
     */
    protected $street;

    /**
     * @var ChangeValue|null
     */
    protected $city;

    /**
     * @var ChangeValue|null
     */
    protected $zip;

    /**
     * @var ChangeValue|null
     */
    protected $country;

    /**
     * @var ChangeValue|null
     */
    protected $email;

    /**
     * @var ChangeValue|null
     */
    protected $phone;

    /**
     * @var ChangeValue|null
     */
    protected $extraLine;

    /**
     * @var ChangeValue|null
     */
    protected $extraAddressLine;

    /**
     * @var ChangeValue|null
     */
    protected $state;

    /**
     * @var ChangeValue|null
     */
    protected $mobile;

    /**
     * @var ChangeValue|null
     */
    protected $fax;

    /**
     * @return ChangeValue|null
     */
    public function getSalutation(): ?ChangeValue
    {
        return $this->salutation;
    }

    /**
     * @param ChangeValue|null $salutation
     * @return AddressChange
     */
    public function setSalutation(?ChangeValue $salutation): AddressChange
    {
        $this->salutation = $salutation;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getFirstName(): ?ChangeValue
    {
        return $this->firstName;
    }

    /**
     * @param ChangeValue|null $firstName
     * @return AddressChange
     */
    public function setFirstName(?ChangeValue $firstName): AddressChange
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getLastName(): ?ChangeValue
    {
        return $this->lastName;
    }

    /**
     * @param ChangeValue|null $lastName
     * @return AddressChange
     */
    public function setLastName(?ChangeValue $lastName): AddressChange
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getCompany(): ?ChangeValue
    {
        return $this->company;
    }

    /**
     * @param ChangeValue|null $company
     * @return AddressChange
     */
    public function setCompany(?ChangeValue $company): AddressChange
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getStreet(): ?ChangeValue
    {
        return $this->street;
    }

    /**
     * @param ChangeValue|null $street
     * @return AddressChange
     */
    public function setStreet(?ChangeValue $street): AddressChange
    {
        $this->street = $street;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getCity(): ?ChangeValue
    {
        return $this->city;
    }

    /**
     * @param ChangeValue|null $city
     * @return AddressChange
     */
    public function setCity(?ChangeValue $city): AddressChange
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getZip(): ?ChangeValue
    {
        return $this->zip;
    }

    /**
     * @param ChangeValue|null $zip
     * @return AddressChange
     */
    public function setZip(?ChangeValue $zip): AddressChange
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getCountry(): ?ChangeValue
    {
        return $this->country;
    }

    /**
     * @param ChangeValue|null $country
     * @return AddressChange
     */
    public function setCountry(?ChangeValue $country): AddressChange
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getEmail(): ?ChangeValue
    {
        return $this->email;
    }

    /**
     * @param ChangeValue|null $email
     * @return AddressChange
     */
    public function setEmail(?ChangeValue $email): AddressChange
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getPhone(): ?ChangeValue
    {
        return $this->phone;
    }

    /**
     * @param ChangeValue|null $phone
     * @return AddressChange
     */
    public function setPhone(?ChangeValue $phone): AddressChange
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getExtraLine(): ?ChangeValue
    {
        return $this->extraLine;
    }

    /**
     * @param ChangeValue|null $extraLine
     * @return AddressChange
     */
    public function setExtraLine(?ChangeValue $extraLine): AddressChange
    {
        $this->extraLine = $extraLine;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getExtraAddressLine(): ?ChangeValue
    {
        return $this->extraAddressLine;
    }

    /**
     * @param ChangeValue|null $extraAddressLine
     * @return AddressChange
     */
    public function setExtraAddressLine(?ChangeValue $extraAddressLine): AddressChange
    {
        $this->extraAddressLine = $extraAddressLine;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getState(): ?ChangeValue
    {
        return $this->state;
    }

    /**
     * @param ChangeValue|null $state
     * @return AddressChange
     */
    public function setState(?ChangeValue $state): AddressChange
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getMobile(): ?ChangeValue
    {
        return $this->mobile;
    }

    /**
     * @param ChangeValue|null $mobile
     * @return AddressChange
     */
    public function setMobile(?ChangeValue $mobile): AddressChange
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getFax(): ?ChangeValue
    {
        return $this->fax;
    }

    /**
     * @param ChangeValue|null $fax
     * @return AddressChange
     */
    public function setFax(?ChangeValue $fax): AddressChange
    {
        $this->fax = $fax;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('salutation', ChangeValue::class, null, true),
            new PropertyInfo('firstName', ChangeValue::class, null, true),
            new PropertyInfo('lastName', ChangeValue::class, null, true),
            new PropertyInfo('company', ChangeValue::class, null, true),
            new PropertyInfo('street', ChangeValue::class, null, true),
            new PropertyInfo('city', ChangeValue::class, null, true),
            new PropertyInfo('zip', ChangeValue::class, null, true),
            new PropertyInfo('country', ChangeValue::class, null, true),
            new PropertyInfo('email', ChangeValue::class, null, true),
            new PropertyInfo('phone', ChangeValue::class, null, true),
            new PropertyInfo('extraLine', ChangeValue::class, null, true),
            new PropertyInfo('extraAddressLine', ChangeValue::class, null, true),
            new PropertyInfo('state', ChangeValue::class, null, true),
            new PropertyInfo('mobile', ChangeValue::class, null, true),
            new PropertyInfo('fax', ChangeValue::class, null, true)
        ]);
    }
}
