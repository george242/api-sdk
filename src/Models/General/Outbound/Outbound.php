<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Outbound;

use DateTime;
use DateTimeZone;
use Exception;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\BaseReturn;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attribute;
use Jtl\Fulfillment\Api\Sdk\Models\General\Address;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attachment;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Outbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Outbound
 */
class Outbound extends DataModel
{
    public const STATUS_PREPARATION = 'Preparation';
    public const STATUS_PENDING = 'Pending';
    public const STATUS_ACKNOWLEDGED = 'Acknowledged';
    public const STATUS_PICKPROCESS = 'Pickprocess';
    public const STATUS_LOCKED = 'Locked';
    public const STATUS_PARTIALLYSHIPPED = 'PartiallyShipped';
    public const STATUS_SHIPPED = 'Shipped';
    public const STATUS_PARTIALLYCANCELED = 'PartiallyCanceled';
    public const STATUS_CANCELED = 'Canceled';
    
    public const PREMIUM_TYPE_EBAY = 'EbayPlus';
    public const PREMIUM_TYPE_AMAZON = 'AmazonSellerFulfilledPrime';
    
    public const SHIPPING_TYPE_STANDARD = 'Standard';
    public const SHIPPING_TYPE_EXPEDITED = 'Expedited';
    public const SHIPPING_TYPE_NEXT_DAY = 'NextDay';
    public const SHIPPING_TYPE_SECOND_DAY = 'SecondDay';
    public const SHIPPING_TYPE_SAME_DAY = 'SameDay';
    public const SHIPPING_TYPE_PROVIDER = 'ByShippingLabelProvider';
    
    /**
     * @var string|null
     */
    protected $outboundId;
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $cancelReason;

    /**
     * @var string|null
     */
    protected $cancelReasonCode;
    
    /**
     * @var Attachment[]
     */
    protected $attachments = [];
    
    /**
     * @var OutboundItem[]
     */
    protected $items = [];
    
    /**
     * @var string|null
     */
    protected $status = self::STATUS_PENDING;
    
    /**
     * @var string|null
     */
    protected $merchantOutboundNumber;
    
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var string|null
     */
    protected $externalNumber;
    
    /**
     * @var string|null
     */
    protected $currency;
    
    /**
     * @var string|null
     */
    protected $internalNote;
    
    /**
     * @var string|null
     */
    protected $externalNote;
    
    /**
     * @var int|null
     */
    protected $priority;
    
    /**
     * @var string|null
     */
    protected $premiumType;
    
    /**
     * @var string|null
     */
    protected $salesChannel;

    /**
     * @var float
     */
    protected $shippingFee;

    /**
     * @var float
     */
    protected $orderValue;
    
    /**
     * @var Attribute[]
     */
    protected $attributes = [];
    
    /**
     * @var DateTime|null
     */
    protected $desiredDeliveryDate;
    
    /**
     * @var string|null
     */
    protected $shippingMethodId;
    
    /**
     * @var string|null
     */
    protected $shippingType;
    
    /**
     * @var Address|null
     */
    protected $shippingAddress;
    
    /**
     * @var Address|null
     */
    protected $senderAddress;

    /**
     * @var OutboundStatusTimestamp|null
     */
    protected $statusTimestamp;

    /**
     * @var BaseReturn[]
     */
    protected $relatedReturns = [];
    
    /**
     * @return string|null
     */
    public function getOutboundId(): ?string
    {
        return $this->outboundId;
    }
    
    /**
     * @param string|null $outboundId
     * @return \Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\Outbound
     */
    public function setOutboundId(?string $outboundId): Outbound
    {
        $this->outboundId = $outboundId;
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Outbound
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Outbound
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCancelReason(): ?string
    {
        return $this->cancelReason;
    }
    
    /**
     * @param string|null $cancelReason
     * @return Outbound
     */
    public function setCancelReason(?string $cancelReason): Outbound
    {
        $this->cancelReason = $cancelReason;
        
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCancelReasonCode(): ?string
    {
        return $this->cancelReasonCode;
    }

    /**
     * @param string|null $cancelReasonCode
     * @return Outbound
     */
    public function setCancelReasonCode(?string $cancelReasonCode): Outbound
    {
        $this->cancelReasonCode = $cancelReasonCode;

        return $this;
    }
    
    /**
     * @return Attachment[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }
    
    /**
     * @param Attachment[] $attachments
     * @return Outbound
     */
    public function setAttachments(array $attachments): Outbound
    {
        $this->attachments = $attachments;
        
        return $this;
    }
    
    /**
     * @param Attachment $attachment
     * @param string|null $key
     * @return Outbound
     */
    public function addAttachment(Attachment $attachment, string $key = null): Outbound
    {
        if ($key === null) {
            $this->attachments[] = $attachment;
        } else {
            $this->attachments[$key] = $attachment;
        }
        
        return $this;
    }
    
    /**
     * @return OutboundItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param OutboundItem[] $items
     * @return Outbound
     */
    public function setItems(array $items): Outbound
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param OutboundItem $item
     * @param string|null $key
     * @return Outbound
     */
    public function addItem(OutboundItem $item, string $key = null): Outbound
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }
    
    /**
     * @param string|null $status
     * @return Outbound
     */
    public function setStatus(?string $status): Outbound
    {
        $this->status = $status;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantOutboundNumber(): ?string
    {
        return $this->merchantOutboundNumber;
    }
    
    /**
     * @param string|null $merchantOutboundNumber
     * @return Outbound
     */
    public function setMerchantOutboundNumber(?string $merchantOutboundNumber): Outbound
    {
        $this->merchantOutboundNumber = $merchantOutboundNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return Outbound
     */
    public function setWarehouseId(?string $warehouseId): Outbound
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExternalNumber(): ?string
    {
        return $this->externalNumber;
    }
    
    /**
     * @param string|null $externalNumber
     * @return Outbound
     */
    public function setExternalNumber(?string $externalNumber): Outbound
    {
        $this->externalNumber = $externalNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }
    
    /**
     * @param string|null $currency
     * @return Outbound
     */
    public function setCurrency(?string $currency): Outbound
    {
        $this->currency = $currency;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getInternalNote(): ?string
    {
        return $this->internalNote;
    }
    
    /**
     * @param string|null $internalNote
     * @return Outbound
     */
    public function setInternalNote(?string $internalNote): Outbound
    {
        $this->internalNote = $internalNote;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExternalNote(): ?string
    {
        return $this->externalNote;
    }
    
    /**
     * @param string|null $externalNote
     * @return Outbound
     */
    public function setExternalNote(?string $externalNote): Outbound
    {
        $this->externalNote = $externalNote;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }
    
    /**
     * @param int|null $priority
     * @return Outbound
     */
    public function setPriority(?int $priority): Outbound
    {
        $this->priority = $priority;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPremiumType(): ?string
    {
        return $this->premiumType;
    }
    
    /**
     * @param string|null $premiumType
     * @return Outbound
     */
    public function setPremiumType(?string $premiumType): Outbound
    {
        $this->premiumType = $premiumType;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getSalesChannel(): ?string
    {
        return $this->salesChannel;
    }
    
    /**
     * @param string|null $salesChannel
     * @return Outbound
     */
    public function setSalesChannel(?string $salesChannel): Outbound
    {
        $this->salesChannel = $salesChannel;
        
        return $this;
    }

    /**
     * @return float|null
     */
    public function getShippingFee(): ?float
    {
        return $this->shippingFee;
    }

    /**
     * @param float $shippingFee
     * @return Outbound
     */
    public function setShippingFee(float $shippingFee): Outbound
    {
        $this->shippingFee = $shippingFee;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getOrderValue(): ?float
    {
        return $this->orderValue;
    }

    /**
     * @param float $orderValue
     * @return Outbound
     */
    public function setOrderValue(float $orderValue): Outbound
    {
        $this->orderValue = $orderValue;

        return $this;
    }
    
    /**
     * @return Attribute[]
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }
    
    /**
     * @param Attribute[] $attributes
     * @return Outbound
     */
    public function setAttributes(array $attributes): Outbound
    {
        $this->attributes = $attributes;
        
        return $this;
    }
    
    /**
     * @param Attribute $attribute
     * @param string|null $key
     * @return Outbound
     */
    public function addAttribute(Attribute $attribute, string $key = null): Outbound
    {
        if ($key === null) {
            $this->attributes[] = $attribute;
        } else {
            $this->attributes[$key] = $attribute;
        }
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getDesiredDeliveryDate(): ?DateTime
    {
        return $this->desiredDeliveryDate;
    }
    
    /**
     * @param DateTime|string|null $desiredDeliveryDate
     * @return Outbound
     * @throws Exception
     */
    public function setDesiredDeliveryDate($desiredDeliveryDate): Outbound
    {
        if ($desiredDeliveryDate === null) {
            return $this;
        }
    
        if (is_string($desiredDeliveryDate)) {
            $desiredDeliveryDate = (new DateTime($desiredDeliveryDate))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($desiredDeliveryDate);
        $this->desiredDeliveryDate = $desiredDeliveryDate;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getShippingMethodId(): ?string
    {
        return $this->shippingMethodId;
    }
    
    /**
     * @param string|null $shippingMethodId
     * @return Outbound
     */
    public function setShippingMethodId(?string $shippingMethodId): Outbound
    {
        $this->shippingMethodId = $shippingMethodId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getShippingType(): ?string
    {
        return $this->shippingType;
    }
    
    /**
     * @param string|null $shippingType
     * @return Outbound
     */
    public function setShippingType(?string $shippingType): Outbound
    {
        $this->shippingType = $shippingType;
        
        return $this;
    }
    
    /**
     * @return Address|null
     */
    public function getShippingAddress(): ?Address
    {
        return $this->shippingAddress;
    }
    
    /**
     * @param Address $shippingAddress
     * @return Outbound
     */
    public function setShippingAddress(Address $shippingAddress): Outbound
    {
        $this->shippingAddress = $shippingAddress;
        
        return $this;
    }
    
    /**
     * @return Address|null
     */
    public function getSenderAddress(): ?Address
    {
        return $this->senderAddress;
    }
    
    /**
     * @param Address $senderAddress
     * @return Outbound
     */
    public function setSenderAddress(Address $senderAddress): Outbound
    {
        $this->senderAddress = $senderAddress;
        
        return $this;
    }

    /**
     * @return OutboundStatusTimestamp|null
     */
    public function getStatusTimestamp(): ?OutboundStatusTimestamp
    {
        return $this->statusTimestamp;
    }

    /**
     * @param OutboundStatusTimestamp|null $statusTimestamp
     * @return Outbound
     */
    public function setStatusTimestamp(?OutboundStatusTimestamp $statusTimestamp): Outbound
    {
        $this->statusTimestamp = $statusTimestamp;

        return $this;
    }

    /**
     * @return BaseReturn[]
     */
    public function getRelatedReturns(): array
    {
        return $this->relatedReturns;
    }

    /**
     * @param BaseReturn[] $relatedReturns
     * @return Outbound
     */
    public function setRelatedReturns(array $relatedReturns): Outbound
    {
        $this->relatedReturns = $relatedReturns;

        return $this;
    }

    /**
     * @param BaseReturn $return
     * @param string|null $key
     * @return Outbound
     */
    public function addRelatedReturn(BaseReturn $return, string $key = null): Outbound
    {
        if ($key === null) {
            $this->relatedReturns[] = $return;
        } else {
            $this->relatedReturns[$key] = $return;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('outboundId', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('cancelReason', 'string', null),
            new PropertyInfo('cancelReasonCode', 'string', null),
            new PropertyInfo('attachments', Attachment::class, [], true, true),
            new PropertyInfo('items', OutboundItem::class, [], true, true),
            new PropertyInfo('status', 'string', self::STATUS_PENDING),
            new PropertyInfo('merchantOutboundNumber', 'string', null),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('externalNumber', 'string', null),
            new PropertyInfo('currency', 'string', null),
            new PropertyInfo('internalNote', 'string', null),
            new PropertyInfo('externalNote', 'string', null),
            new PropertyInfo('priority', 'int', null),
            new PropertyInfo('premiumType', 'string', null),
            new PropertyInfo('salesChannel', 'string', null),
            new PropertyInfo('shippingFee', 'float', null),
            new PropertyInfo('orderValue', 'float', null),
            new PropertyInfo('attributes', Attribute::class, [], true, true),
            new PropertyInfo('desiredDeliveryDate', DateTime::class, null),
            new PropertyInfo('shippingType', 'string', null),
            new PropertyInfo('shippingAddress', Address::class, null, true),
            new PropertyInfo('shippingMethodId', 'string', null),
            new PropertyInfo('senderAddress', Address::class, null, true),
            new PropertyInfo('statusTimestamp', OutboundStatusTimestamp::class, null, true),
            new PropertyInfo('relatedReturns', BaseReturn::class, [], true, true),
        ]);
    }
}
