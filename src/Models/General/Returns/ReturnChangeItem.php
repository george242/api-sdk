<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;

/**
 * Class ReturnChangeItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnChangeItem extends DataModel
{
    /**
     * @var ChangeValue|null
     */
    protected $returnItemId;

    /**
     * @var ChangeValue|null
     */
    protected $jfsku;

    /**
     * @var ChangeValue|null
     */
    protected $outboundId;

    /**
     * @var ChangeValue|null
     */
    protected $outboundItemId;

    /**
     * @var ChangeValue|null
     */
    protected $quantity;

    /**
     * @var ChangeValue|null
     */
    protected $reason;

    /**
     * @var ChangeValue|null
     */
    protected $reasonNote;

    /**
     * @var ChangeValue|null
     */
    protected $conditionNote;

    /**
     * @var ChangeValue|null
     */
    protected $state;

    /**
     * @var ChangeValue|null
     */
    protected $condition;

    /**
     * @var ReturnChangeItemStockShell[]
     */
    protected $stockChanges = [];

    /**
     * @return ChangeValue|null
     */
    public function getReturnItemId(): ?ChangeValue
    {
        return $this->returnItemId;
    }

    /**
     * @param ChangeValue|null $returnItemId
     * @return ReturnChangeItem
     */
    public function setReturnItemId(?ChangeValue $returnItemId): ReturnChangeItem
    {
        $this->returnItemId = $returnItemId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getJfsku(): ?ChangeValue
    {
        return $this->jfsku;
    }

    /**
     * @param ChangeValue|null $jfsku
     * @return ReturnChangeItem
     */
    public function setJfsku(?ChangeValue $jfsku): ReturnChangeItem
    {
        $this->jfsku = $jfsku;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getOutboundId(): ?ChangeValue
    {
        return $this->outboundId;
    }

    /**
     * @param ChangeValue|null $outboundId
     * @return ReturnChangeItem
     */
    public function setOutboundId(?ChangeValue $outboundId): ReturnChangeItem
    {
        $this->outboundId = $outboundId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getOutboundItemId(): ?ChangeValue
    {
        return $this->outboundItemId;
    }

    /**
     * @param ChangeValue|null $outboundItemId
     * @return ReturnChangeItem
     */
    public function setOutboundItemId(?ChangeValue $outboundItemId): ReturnChangeItem
    {
        $this->outboundItemId = $outboundItemId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getQuantity(): ?ChangeValue
    {
        return $this->quantity;
    }

    /**
     * @param ChangeValue|null $quantity
     * @return ReturnChangeItem
     */
    public function setQuantity(?ChangeValue $quantity): ReturnChangeItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getReason(): ?ChangeValue
    {
        return $this->reason;
    }

    /**
     * @param ChangeValue|null $reason
     * @return ReturnChangeItem
     */
    public function setReason(?ChangeValue $reason): ReturnChangeItem
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getReasonNote(): ?ChangeValue
    {
        return $this->reasonNote;
    }

    /**
     * @param ChangeValue|null $reasonNote
     * @return ReturnChangeItem
     */
    public function setReasonNote(?ChangeValue $reasonNote): ReturnChangeItem
    {
        $this->reasonNote = $reasonNote;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getConditionNote(): ?ChangeValue
    {
        return $this->conditionNote;
    }

    /**
     * @param ChangeValue|null $conditionNote
     * @return ReturnChangeItem
     */
    public function setConditionNote(?ChangeValue $conditionNote): ReturnChangeItem
    {
        $this->conditionNote = $conditionNote;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getState(): ?ChangeValue
    {
        return $this->state;
    }

    /**
     * @param ChangeValue|null $state
     * @return ReturnChangeItem
     */
    public function setState(?ChangeValue $state): ReturnChangeItem
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getCondition(): ?ChangeValue
    {
        return $this->condition;
    }

    /**
     * @param ChangeValue|null $condition
     * @return ReturnChangeItem
     */
    public function setCondition(?ChangeValue $condition): ReturnChangeItem
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return ReturnChangeItemStockShell[]
     */
    public function getStockChanges(): array
    {
        return $this->stockChanges;
    }

    /**
     * @param ReturnChangeItemStockShell[] $stockChanges
     * @return ReturnChangeItem
     */
    public function setStockChanges(array $stockChanges): ReturnChangeItem
    {
        $this->stockChanges = $stockChanges;
        return $this;
    }

    /**
     * @param ReturnChangeItemStockShell $stockChange
     * @param string|null $key
     * @return ReturnChangeItem
     */
    public function addStockChange(ReturnChangeItemStockShell $stockChange, string $key = null): ReturnChangeItem
    {
        if ($key === null) {
            $this->stockChanges[] = $stockChange;
        } else {
            $this->stockChanges[$key] = $stockChange;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('returnItemId', ChangeValue::class, null, true),
            new PropertyInfo('jfsku', ChangeValue::class, null, true),
            new PropertyInfo('outboundId', ChangeValue::class, null, true),
            new PropertyInfo('outboundItemId', ChangeValue::class, null, true),
            new PropertyInfo('quantity', ChangeValue::class, null, true),
            new PropertyInfo('reason', ChangeValue::class, null, true),
            new PropertyInfo('reasonNote', ChangeValue::class, null, true),
            new PropertyInfo('conditionNote', ChangeValue::class, null, true),
            new PropertyInfo('state', ChangeValue::class, null, true),
            new PropertyInfo('condition', ChangeValue::class, null, true),
            new PropertyInfo('stockChanges', ReturnChangeItemStockShell::class, [], true, true),
        ]);
    }
}
