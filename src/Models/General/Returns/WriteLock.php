<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use DateTimeZone;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use DateTime;
use Exception;

/**
 * Class WriteLock
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class WriteLock extends DataModel
{
    /**
     * @var string|null
     */
    protected $lockedByUserId;

    /**
     * @var DateTime|null
     */
    protected $lockedAt;

    /**
     * @var DateTime|null
     */
    protected $lockedUntil;

    /**
     * @return string|null
     */
    public function getLockedByUserId(): ?string
    {
        return $this->lockedByUserId;
    }

    /**
     * @param string|null $lockedByUserId
     * @return WriteLock
     */
    public function setLockedByUserId(?string $lockedByUserId): WriteLock
    {
        $this->lockedByUserId = $lockedByUserId;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLockedAt(): ?DateTime
    {
        return $this->lockedAt;
    }

    /**
     * @param DateTime|string|null $lockedAt
     * @return WriteLock
     * @throws Exception
     */
    public function setLockedAt(?DateTime $lockedAt): WriteLock
    {
        if ($lockedAt === null) {
            return $this;
        }

        if (is_string($lockedAt)) {
            $lockedAt = (new DateTime($lockedAt))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($lockedAt);
        $this->lockedAt = $lockedAt;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLockedUntil(): ?DateTime
    {
        return $this->lockedUntil;
    }

    /**
     * @param DateTime|string|null $lockedUntil
     * @return WriteLock
     * @throws Exception
     */
    public function setLockedUntil(?DateTime $lockedUntil): WriteLock
    {
        if ($lockedUntil === null) {
            return $this;
        }

        if (is_string($lockedUntil)) {
            $lockedUntil = (new DateTime($lockedUntil))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($lockedUntil);
        $this->lockedUntil = $lockedUntil;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('lockedByUserId', 'string', null),
            new PropertyInfo('lockedAt', DateTime::class, null),
            new PropertyInfo('lockedUntil', DateTime::class, null),
        ]);
    }
}
