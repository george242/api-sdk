<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValueInt;

/**
 * Class ReturnStockChangeIdChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnStockChangeIdChange extends DataModel
{
    /**
     * @var ChangeValue|null
     */
    protected $warehouseId;

    /**
     * @var ChangeValue|null
     */
    protected $jfsku;

    /**
     * @var ChangeValueInt|null
     */
    protected $stockVersion;

    /**
     * @return ChangeValue|null
     */
    public function getWarehouseId(): ?ChangeValue
    {
        return $this->warehouseId;
    }

    /**
     * @param ChangeValue|null $warehouseId
     * @return ReturnStockChangeIdChange
     */
    public function setWarehouseId(?ChangeValue $warehouseId): ReturnStockChangeIdChange
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getJfsku(): ?ChangeValue
    {
        return $this->jfsku;
    }

    /**
     * @param ChangeValue|null $jfsku
     * @return ReturnStockChangeIdChange
     */
    public function setJfsku(?ChangeValue $jfsku): ReturnStockChangeIdChange
    {
        $this->jfsku = $jfsku;
        return $this;
    }

    /**
     * @return ChangeValueInt|null
     */
    public function getStockVersion(): ?ChangeValueInt
    {
        return $this->stockVersion;
    }

    /**
     * @param ChangeValueInt|null $stockVersion
     * @return ReturnStockChangeIdChange
     */
    public function setStockVersion(?ChangeValueInt $stockVersion): ReturnStockChangeIdChange
    {
        $this->stockVersion = $stockVersion;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('warehouseId', ChangeValue::class, null, true),
            new PropertyInfo('jfsku', ChangeValue::class, null, true),
            new PropertyInfo('stockVersion', ChangeValueInt::class, null, true),
        ]);
    }
}
