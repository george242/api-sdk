<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Inbound\Inbound as GeneralInbound;

/**
 * Class Inbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound
 */
class Inbound extends GeneralInbound
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return Inbound
     */
    public function setMerchantId(?string $merchantId): Inbound
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null)
        ]);
    }
}
