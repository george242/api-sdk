<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * Class ReturnIncomingItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns
 */
class ReturnIncomingItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $warehouseId;

    /**
     * @var string|null
     */
    protected $fulfillerStockChangeId;

    /**
     * @var float|null
     */
    protected $quantityBlocked;

    /**
     * @var float|null
     */
    protected $quantity;

    /**
     * @var string|null
     */
    protected $note;

    /**
     * @var string|null
     */
    protected $batch;

    /**
     * @var DateTime|null
     */
    protected $fulfillerTimestamp;

    /**
     * @var BestBefore|null
     */
    protected $bestBefore;

    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    /**
     * @param string|null $warehouseId
     * @return ReturnIncomingItem
     */
    public function setWarehouseId(?string $warehouseId): ReturnIncomingItem
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFulfillerStockChangeId(): ?string
    {
        return $this->fulfillerStockChangeId;
    }

    /**
     * @param string|null $fulfillerStockChangeId
     * @return ReturnIncomingItem
     */
    public function setFulfillerStockChangeId(?string $fulfillerStockChangeId): ReturnIncomingItem
    {
        $this->fulfillerStockChangeId = $fulfillerStockChangeId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantityBlocked(): ?float
    {
        return $this->quantityBlocked;
    }

    /**
     * @param float|null $quantityBlocked
     * @return ReturnIncomingItem
     */
    public function setQuantityBlocked(?float $quantityBlocked): ReturnIncomingItem
    {
        $this->quantityBlocked = $quantityBlocked;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     * @return ReturnIncomingItem
     */
    public function setQuantity(?float $quantity): ReturnIncomingItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     * @return ReturnIncomingItem
     */
    public function setNote(?string $note): ReturnIncomingItem
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }

    /**
     * @param string|null $batch
     * @return ReturnIncomingItem
     */
    public function setBatch(?string $batch): ReturnIncomingItem
    {
        $this->batch = $batch;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getFulfillerTimestamp(): ?DateTime
    {
        return $this->fulfillerTimestamp;
    }

    /**
     * @param DateTime|string|null $fulfillerTimestamp
     * @return ReturnIncomingItem
     * @throws Exception
     */
    public function setFulfillerTimestamp($fulfillerTimestamp): ReturnIncomingItem
    {
        if ($fulfillerTimestamp === null) {
            return $this;
        }

        if (is_string($fulfillerTimestamp)) {
            $fulfillerTimestamp = (new DateTime($fulfillerTimestamp))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($fulfillerTimestamp);
        $this->fulfillerTimestamp = $fulfillerTimestamp;

        return $this;
    }

    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }

    /**
     * @param BestBefore|null $bestBefore
     * @return ReturnIncomingItem
     */
    public function setBestBefore(?BestBefore $bestBefore): ReturnIncomingItem
    {
        $this->bestBefore = $bestBefore;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('fulfillerStockChangeId', 'string', null),
            new PropertyInfo('quantityBlocked', 'float', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('fulfillerTimestamp', DateTime::class, null),
        ]);
    }
}
