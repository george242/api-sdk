<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\FulfillerReturn;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\Outbound as GeneralOutbound;

/**
 * Class Outbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound
 */
class Outbound extends GeneralOutbound
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return Outbound
     */
    public function setMerchantId(?string $merchantId): Outbound
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null),
            new PropertyInfo('relatedReturns', FulfillerReturn::class, [], true, true),
        ]);
    }
}
