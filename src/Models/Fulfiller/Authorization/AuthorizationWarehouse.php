<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class AuthorizationWarehouse
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization
 */
class AuthorizationWarehouse extends DataModel
{
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var AuthorizationShippingMethod[]
     */
    protected $shippingMethods = [];
    
    /**
     * @var DateTime|null
     */
    protected $createdAt;
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return AuthorizationWarehouse
     */
    public function setWarehouseId(?string $warehouseId): AuthorizationWarehouse
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return AuthorizationShippingMethod[]
     */
    public function getShippingMethods(): array
    {
        return $this->shippingMethods;
    }
    
    /**
     * @param AuthorizationShippingMethod[] $shippingMethods
     * @return AuthorizationWarehouse
     */
    public function setShippingMethods(array $shippingMethods): AuthorizationWarehouse
    {
        $this->shippingMethods = $shippingMethods;
        
        return $this;
    }
    
    /**
     * @param AuthorizationShippingMethod $shippingMethod
     * @param string|null $key
     * @return AuthorizationWarehouse
     */
    public function addShippingMethod(AuthorizationShippingMethod $shippingMethod, string $key = null): AuthorizationWarehouse
    {
        if ($key === null) {
            $this->shippingMethods[] = $shippingMethod;
        } else {
            $this->shippingMethods[$key] = $shippingMethod;
        }
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return AuthorizationWarehouse
     * @throws Exception
     */
    public function setCreatedAt($createdAt): AuthorizationWarehouse
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = (new DateTime($createdAt))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($createdAt);
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('shippingMethods', AuthorizationShippingMethod::class, null, true, true),
            new PropertyInfo('createdAt', DateTime::class, null)
        ]);
    }
}
