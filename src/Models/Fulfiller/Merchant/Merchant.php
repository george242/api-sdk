<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Merchant;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Address;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Merchant
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Merchant
 */
class Merchant extends DataModel
{
    /**
     * @var string|null
     */
    protected $userId;
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var Address|null
     */
    protected $address;
    
    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
    
    /**
     * @param string|null $userId
     * @return Merchant
     */
    public function setUserId(?string $userId): Merchant
    {
        $this->userId = $userId;
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Merchant
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Merchant
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }
    
    /**
     * @param Address $address
     * @return Merchant
     */
    public function setAddress(Address $address): Merchant
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('userId', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('address', Address::class, null, true)
        ]);
    }
}
