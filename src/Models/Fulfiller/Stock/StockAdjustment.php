<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;

/**
 * Class StockAdjustment
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock
 */
class StockAdjustment extends DataModel
{
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var float|null
     */
    protected $quantityBlocked;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $batch;
    
    /**
     * @var BestBefore|null
     */
    protected $bestBefore;
    
    /**
     * @var DateTime|null
     */
    protected $fulfillerTimestamp;
    
    /**
     * @var string|null
     */
    protected $fulfillerStockChangeId;
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return StockAdjustment
     */
    public function setJfsku(?string $jfsku): StockAdjustment
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return StockAdjustment
     */
    public function setWarehouseId(?string $warehouseId): StockAdjustment
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityBlocked(): ?float
    {
        return $this->quantityBlocked;
    }
    
    /**
     * @param float|null $quantityBlocked
     * @return StockAdjustment
     */
    public function setQuantityBlocked(?float $quantityBlocked): StockAdjustment
    {
        $this->quantityBlocked = $quantityBlocked;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return StockAdjustment
     */
    public function setQuantity(?float $quantity): StockAdjustment
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return StockAdjustment
     */
    public function setNote(?string $note): StockAdjustment
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }
    
    /**
     * @param string|null $batch
     * @return StockAdjustment
     */
    public function setBatch(?string $batch): StockAdjustment
    {
        $this->batch = $batch;
        
        return $this;
    }
    
    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }
    
    /**
     * @param BestBefore $bestBefore
     * @return StockAdjustment
     */
    public function setBestBefore(BestBefore $bestBefore): StockAdjustment
    {
        $this->bestBefore = $bestBefore;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getFulfillerTimestamp(): ?DateTime
    {
        return $this->fulfillerTimestamp;
    }
    
    /**
     * @param DateTime|string|null $fulfillerTimestamp
     * @return StockAdjustment
     * @throws Exception
     */
    public function setFulfillerTimestamp(DateTime $fulfillerTimestamp): StockAdjustment
    {
        if ($fulfillerTimestamp === null) {
            return $this;
        }
    
        if (is_string($fulfillerTimestamp)) {
            $fulfillerTimestamp = (new DateTime($fulfillerTimestamp))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($fulfillerTimestamp);
        $this->fulfillerTimestamp = $fulfillerTimestamp;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerStockChangeId(): ?string
    {
        return $this->fulfillerStockChangeId;
    }
    
    /**
     * @param string|null $fulfillerStockChangeId
     * @return StockAdjustment
     */
    public function setFulfillerStockChangeId(?string $fulfillerStockChangeId): StockAdjustment
    {
        $this->fulfillerStockChangeId = $fulfillerStockChangeId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('quantityBlocked', 'float', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('fulfillerTimestamp', DateTime::class, null),
            new PropertyInfo('fulfillerStockChangeId', 'string', null)
        ]);
    }
}
