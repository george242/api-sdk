<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockInboundItem as GeneralStockInboundItem;

/**
 * Class StockOutboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock
 */
class StockOutboundItem extends GeneralStockInboundItem
{
    /**
     * @var string|null
     */
    protected $outboundId;
    
    /**
     * @return string|null
     */
    public function getOutboundId(): ?string
    {
        return $this->outboundId;
    }
    
    /**
     * @param string|null $outboundId
     * @return StockOutboundItem
     */
    public function setOutboundId(?string $outboundId): StockOutboundItem
    {
        $this->outboundId = $outboundId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(new PropertyInfo('outboundId', 'string', null));
    }
}
