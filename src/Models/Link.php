<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Link
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
class Link extends DataModel
{
    /**
     * @var string|null
     */
    protected $previous;
    
    /**
     * @var string|null
     */
    protected $next;
    
    /**
     * @return string|null
     */
    public function getPrevious(): ?string
    {
        return $this->previous;
    }
    
    /**
     * @param string|null $previous
     * @return Link
     */
    public function setPrevious(?string $previous): Link
    {
        $this->previous = $previous;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNext(): ?string
    {
        return $this->next;
    }
    
    /**
     * @param string|null $next
     * @return Link
     */
    public function setNext(?string $next): Link
    {
        $this->next = $next;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('previous', 'string', null),
            new PropertyInfo('next', 'string', null)
        ]);
    }
}
