<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class SfpCredential extends DataModel
{
    public const PLATFORM_DE = 'Amazon_de';
    public const PLATFORM_CO_UK = 'Amazon_co_uk';
    public const PLATFORM_FR = 'Amazon_fr';
    public const PLATFORM_IT = 'Amazon_it';
    public const PLATFORM_ES = 'Amazon_es';
    public const PLATFORM_CA = 'Amazon_ca';
    public const PLATFORM_COM = 'Amazon_com';
    public const PLATFORM_COM_MX = 'Amazon_com_mx';
    public const PLATFORM_COM_AU = 'Amazon_com_au';
    
    /**
     * @var string|null
     */
    protected $credentialId;
    
    /**
     * @var string|null
     */
    protected $sellerId;
    
    /**
     * @var string|null
     */
    protected $mwsAuthToken;
    
    /**
     * @var string|null
     */
    protected $platform = self::PLATFORM_DE;
    
    /**
     * @return string|null
     */
    public function getSellerId(): ?string
    {
        return $this->sellerId;
    }
    
    /**
     * @return string|null
     */
    public function getCredentialId(): ?string
    {
        return $this->credentialId;
    }
    
    /**
     * @param string|null $credentialId
     * @return SfpCredential
     */
    public function setCredentialId(?string $credentialId): SfpCredential
    {
        $this->credentialId = $credentialId;
        
        return $this;
    }
    
    /**
     * @param string|null $sellerId
     * @return SfpCredential
     */
    public function setSellerId(?string $sellerId): SfpCredential
    {
        $this->sellerId = $sellerId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMwsAuthToken(): ?string
    {
        return $this->mwsAuthToken;
    }
    
    /**
     * @param string|null $mwsAuthToken
     * @return SfpCredential
     */
    public function setMwsAuthToken(?string $mwsAuthToken): SfpCredential
    {
        $this->mwsAuthToken = $mwsAuthToken;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPlatform(): ?string
    {
        return $this->platform;
    }
    
    /**
     * @param string|null $platform
     * @return SfpCredential
     */
    public function setPlatform(?string $platform): SfpCredential
    {
        $this->platform = $platform;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('credentialId', 'string', null),
            new PropertyInfo('sellerId', 'string', null),
            new PropertyInfo('mwsAuthToken', 'string', null),
            new PropertyInfo('platform', 'string', self::PLATFORM_DE),
        ]);
    }
}
