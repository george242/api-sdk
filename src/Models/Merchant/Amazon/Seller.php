<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class Seller extends DataModel
{
    /**
     * @var string|null
     */
    protected $merchant;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $authToken;

    /**
     * @var bool|null
     */
    protected $isTerminated;

    /**
     * @var string|null
     */
    protected $marketplaceId;

    /**
     * @return string|null
     */
    public function getMerchant(): ?string
    {
        return $this->merchant;
    }

    /**
     * @param string|null $merchant
     * @return Seller
     */
    public function setMerchant(?string $merchant): Seller
    {
        $this->merchant = $merchant;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Seller
     */
    public function setName(?string $name): Seller
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthToken(): ?string
    {
        return $this->authToken;
    }

    /**
     * @param string|null $authToken
     * @return Seller
     */
    public function setAuthToken(?string $authToken): Seller
    {
        $this->authToken = $authToken;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsTerminated(): ?bool
    {
        return $this->isTerminated;
    }

    /**
     * @param bool|null $isTerminated
     * @return Seller
     */
    public function setIsTerminated(?bool $isTerminated): Seller
    {
        $this->isTerminated = $isTerminated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMarketplaceId(): ?string
    {
        return $this->marketplaceId;
    }

    /**
     * @param string|null $marketplaceId
     * @return Seller
     */
    public function setMarketplaceId(?string $marketplaceId): Seller
    {
        $this->marketplaceId = $marketplaceId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('merchant', 'string', null),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('authToken', 'string', null),
            new PropertyInfo('isTerminated', 'bool', false),
            new PropertyInfo('marketplaceId', 'string', null),
        ]);
    }
}
