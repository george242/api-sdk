<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Inbound\Inbound as GeneralInbound;

/**
 * Class Inbound
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound
 */
class Inbound extends GeneralInbound
{
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return Inbound
     */
    public function setFulfillerId(?string $fulfillerId): Inbound
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(new PropertyInfo('fulfillerId', 'string', null));
    }
}
