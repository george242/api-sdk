<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\BaseReturn;
use \Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnItem as MerchantReturnItem;

/**
 * Class MerchantReturn
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns
 */
class MerchantReturn extends BaseReturn
{
    /**
     * @var string|null
     */
    protected $fulfillerId;

    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }

    /**
     * @param string|null $fulfillerId
     * @return MerchantReturn
     */
    public function setFulfillerId(?string $fulfillerId): MerchantReturn
    {
        $this->fulfillerId = $fulfillerId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('fulfillerId', 'string', null),
            new PropertyInfo('items', MerchantReturnItem::class, [], true, true),
        ]);
    }
}
