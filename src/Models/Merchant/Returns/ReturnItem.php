<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnItem as BaseReturnItem;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChange;

/**
 * Class ReturnItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns
 */
class ReturnItem extends BaseReturnItem
{
    /**
     * @var string|null
     */
    protected $merchantSku;

    /**
     * @var ReturnStockChange[]
     */
    protected $stockChanges = [];

    /**
     * @return ReturnStockChange[]
     */
    public function getStockChanges(): array
    {
        return $this->stockChanges;
    }

    /**
     * @param ReturnStockChange[] $stockChanges
     * @return ReturnItem
     */
    public function setStockChanges(array $stockChanges): ReturnItem
    {
        $this->stockChanges = $stockChanges;
        return $this;
    }

    /**
     * @param ReturnStockChange $stockChanges
     * @param string|null $key
     * @return ReturnItem
     */
    public function addStockChange(ReturnStockChange $stockChanges, string $key = null): ReturnItem
    {
        if ($key === null) {
            $this->stockChanges[] = $stockChanges;
        } else {
            $this->stockChanges[$key] = $stockChanges;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('stockChanges', ReturnStockChange::class, [], true, true),
        ]);
    }
}
