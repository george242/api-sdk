<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Chunk
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
class Chunk extends DataModel
{
    /**
     * @var string|null
     */
    protected $nextChunkUrl;
    
    /**
     * @var DateTime|null
     */
    protected $from;
    
    /**
     * @var DateTime|null
     */
    protected $to;
    
    /**
     * @var Model[]
     */
    protected $data;
    
    /**
     * @var bool
     */
    protected $moreDataAvailable = false;
    
    /**
     * @return string|null
     */
    public function getNextChunkUrl(): ?string
    {
        return $this->nextChunkUrl;
    }
    
    /**
     * @param string|null $nextChunkUrl
     * @return Chunk
     */
    public function setNextChunkUrl(?string $nextChunkUrl): Chunk
    {
        $this->nextChunkUrl = $nextChunkUrl;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getFrom(): ?DateTime
    {
        return $this->from;
    }
    
    /**
     * @param DateTime|string|null $from
     * @return Chunk
     * @throws Exception
     */
    public function setFrom($from): Chunk
    {
        if ($from === null) {
            return $this;
        }
    
        if (is_string($from)) {
            $from = (new DateTime($from))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($from);
        $this->from = $from;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getTo(): ?DateTime
    {
        return $this->to;
    }
    
    /**
     * @param DateTime|null|string $to
     * @return Chunk
     * @throws Exception
     */
    public function setTo($to): Chunk
    {
        if ($to === null) {
            return $this;
        }
    
        if (is_string($to)) {
            $to = (new DateTime($to))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($to);
        $this->to = $to;
    
        return $this;
    }
    
    /**
     * @return Model[]
     */
    public function getData(): array
    {
        return $this->data;
    }
    
    /**
     * @param Model[] $data
     * @return Chunk
     */
    public function setData(array $data): Chunk
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isMoreDataAvailable(): bool
    {
        return $this->moreDataAvailable;
    }
    
    /**
     * @param bool $moreDataAvailable
     * @return Chunk
     */
    public function setMoreDataAvailable(bool $moreDataAvailable): Chunk
    {
        $this->moreDataAvailable = $moreDataAvailable;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('nextChunkUrl', 'string', null),
            new PropertyInfo('from', DateTime::class, null),
            new PropertyInfo('to', DateTime::class, null),
            new PropertyInfo('data', 'mixed', []),
            new PropertyInfo('moreDataAvailable', 'bool', false),
        ]);
    }
}
