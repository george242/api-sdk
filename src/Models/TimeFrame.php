<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class TimeFrame extends DataModel
{
    /**
     * @var Model[]
     */
    protected $data = [];
    
    /**
     * @var string
     */
    protected $nextChunkUrl = '';
    
    /**
     * @var DateTime|null
     */
    protected $from;
    
    /**
     * @var DateTime|null
     */
    protected $to;
    
    /**
     * @return Model[]
     */
    public function getData(): array
    {
        return $this->data;
    }
    
    /**
     * @param Model[] $data
     * @return TimeFrame
     */
    public function setData(array $data): TimeFrame
    {
        $this->data = $data;
        
        return $this;
    }
    
    /**
     * @return string
     */
    public function getNextChunkUrl(): string
    {
        return $this->nextChunkUrl;
    }
    
    /**
     * @param string $nextChunkUrl
     * @return TimeFrame
     */
    public function setNextChunkUrl(string $nextChunkUrl): TimeFrame
    {
        $this->nextChunkUrl = $nextChunkUrl;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getFrom(): ?DateTime
    {
        return $this->from;
    }
    
    /**
     * @param DateTime|null|string $from
     * @return TimeFrame
     * @throws Exception
     */
    public function setFrom($from): TimeFrame
    {
        if ($from === null) {
            return $this;
        }
    
        if (is_string($from)) {
            $from = (new DateTime($from))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($from);
        $this->from = $from;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getTo(): ?DateTime
    {
        return $this->to;
    }
    
    /**
     * @param DateTime|null|string $to
     * @return TimeFrame
     * @throws Exception
     */
    public function setTo($to): TimeFrame
    {
        if ($to === null) {
            return $this;
        }
    
        if (is_string($to)) {
            $to = (new DateTime($to))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($to);
        $this->to = $to;
    
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('data', 'mixed', []),
            new PropertyInfo('from', DateTime::class, null),
            new PropertyInfo('to', DateTime::class, null),
            new PropertyInfo('nextChunkUrl')
        ]);
    }
}
