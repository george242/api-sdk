<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Merchant\Merchant;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class MerchantResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class MerchantResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/merchants', Merchant::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $merchantId
     * @return Model|Merchant|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findById(string $merchantId): ?Merchant
    {
        return $this->findBy(
            sprintf('fulfiller/merchants/%s', $merchantId),
            Merchant::class,
            $this->buildCacheKey($merchantId)
        );
    }
}
