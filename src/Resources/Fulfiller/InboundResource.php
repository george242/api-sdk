<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\InboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\IncomingGood;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\IncomingGoodItem;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class InboundResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class InboundResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/inbounds', Inbound::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Inbound|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('fulfiller/inbounds/%s', $id), Inbound::class, $this->buildCacheKey($id), $query);
    }
    
    /**
     * @param string $inboundId
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    //public function findShippingNotifications(string $inboundId): array
    public function findShippingNotifications(string $inboundId): Pagination
    {
        $query = new Query();
    
        return $this->findAll(
            sprintf('fulfiller/inbounds/%s/shipping-notifications', $inboundId),
            InboundShippingNotification::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
        
        /*
        $result = [];

        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/inbounds/%s/shipping-notifications', $inboundId)
            );

            $data = $this->extractData($response);
            foreach ($data as $d) {
                $result[] = new InboundShippingNotification($d);
            }
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return $result;
        */
    }
    
    /**
     * @param string $inboundId
     * @param string $merchantShippingNotificationNumber
     * @return InboundShippingNotification|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationById(
        string $inboundId,
        string $merchantShippingNotificationNumber
    ): ?InboundShippingNotification {
        $cacheKey = $this->buildCacheKey($merchantShippingNotificationNumber);
    
        // Try Cache
        $cachedItem = $this->getResourceCache()->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('fulfiller/inbounds/%s/shipping-notifications/%s', $inboundId, $merchantShippingNotificationNumber)
            );
    
            $notification = new InboundShippingNotification($this->extractData($response));
    
            // Set Cache
            $this->getResourceCache()->set($notification, $cacheKey);
    
            return $notification;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param string $inboundId
     * @param IncomingGoodItem $item
     * @return bool
     * @throws Throwable
     */
    public function itemArrived(string $inboundId, IncomingGoodItem $item): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/inbounds/%s/incoming-goods', $inboundId),
                [
                    'body' => json_encode($item)
                ]
            );
        
            return $response->getStatusCode() === 201;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param string $inboundId
     * @param IncomingGood $good
     * @return bool
     * @throws Throwable
     */
    public function itemsArrived(string $inboundId, IncomingGood $good): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('fulfiller/inbounds/%s/incoming-goods/bulk', $inboundId),
                [
                    'body' => json_encode($good)
                ]
            );
            
            return $response->getStatusCode() === 201;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
    
    /**
     * @param string $inboundId
     * @return bool
     * @throws Throwable
     */
    public function close(string $inboundId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('fulfiller/inbounds/%s/close', $inboundId)
            );
    
            $result = $response->getStatusCode() === 200;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($inboundId));
        
                // Delete Page Cache
                $this->deletePageCache();
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
}
