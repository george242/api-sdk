<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use InvalidArgumentException;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attachment;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\OutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\Outbound;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class OutboundResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class OutboundResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/outbounds', Outbound::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $outboundId
     * @param Query|null $query
     * @return Outbound|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $outboundId, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('merchant/outbounds/%s', $outboundId), Outbound::class, $this->buildCacheKey($outboundId), $query);
    }
    
    /**
     * @param Outbound|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Outbound::class);
        
        $queryString = $query !== null ? $query->buildQueryString() : '';
        
        /** @var Outbound $model */
        return empty($model->getOutboundId()) ?
            $this->create($model, 'merchant/outbounds' . $queryString, Outbound::class, $model->property('outboundId')) :
            $this->update(
                $model,
                sprintf('merchant/outbounds/%s' . $queryString, $model->getOutboundId()),
                $this->buildCacheKey($model->getOutboundId())
            );
    }
    
    /**
     * @param string $outboundId
     * @param string $status
     * @return bool
     * @throws Throwable
     */
    public function changeStatus(string $outboundId, string $status): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('merchant/outbounds/%s/status', $outboundId),
                [
                    'body' => json_encode(['status' => $status])
                ]
            );
    
            $result = $response->getStatusCode() === 204;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param string $outboundId
     * @param string $merchantDocumentId
     * @param string $documentType
     * @param string $data - Base64 Coded String
     * @return Attachment|null
     * @throws JsonException
     * @throws Throwable
     */
    public function addAttachment(
        string $outboundId,
        string $merchantDocumentId,
        string $documentType,
        string $data
    ): ?Attachment {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('merchant/outbounds/%s/attachments', $outboundId),
                [
                    'body' => json_encode([
                        'data' => $data,
                        'merchantDocumentId' => $merchantDocumentId,
                        'documentType' => $documentType
                    ])
                ]
            );
    
            $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
    
            // Delete Page Cache
            $this->deletePageCache();
        
            return new Attachment($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $outboundId
     * @param string $merchantDocumentId
     * @return Attachment|null
     * @throws Throwable
     */
    public function findAttachment(string $outboundId, string $merchantDocumentId): ?Attachment
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('merchant/outbounds/%s/attachments/%s', $outboundId, $merchantDocumentId)
            );
        
            return new Attachment($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return null;
    }
    
    /**
     * @param string $outboundId
     * @param string $merchantDocumentId
     * @return bool
     * @throws Throwable
     */
    public function removeAttachment(string $outboundId, string $merchantDocumentId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf('merchant/outbounds/%s/attachments/%s', $outboundId, $merchantDocumentId)
            );
    
            $result = $response->getStatusCode() === 200;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($outboundId));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/outbounds/updates',
            Outbound::class,
            $query
        );
    }
    
    /**
     * @param string $outboundId
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotifications(string $outboundId): Pagination
    {
        $query = new Query();
        
        return $this->findAll(
            sprintf('merchant/outbounds/%s/shipping-notifications', $outboundId),
            OutboundShippingNotification::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param string $outboundId
     * @param string $outboundShippingNotificationId
     * @return OutboundShippingNotification|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationById(
        string $outboundId,
        string $outboundShippingNotificationId
    ): ?OutboundShippingNotification {
        $cacheKey = $this->buildCacheKey($outboundShippingNotificationId);
    
        // Try Cache
        $cachedItem = $this->getResourceCache()->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('merchant/outbounds/%s/shipping-notifications/%s', $outboundId, $outboundShippingNotificationId)
            );
    
            $notification = new OutboundShippingNotification($this->extractData($response));
    
            // Set Cache
            $this->getResourceCache()->set($notification, $cacheKey);
    
            return $notification;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param OutboundShippingNotification $notification
     * @return bool
     * @throws Throwable
     */
    public function updateShippingNotification(OutboundShippingNotification $notification): bool
    {
        if (empty($notification->getOutboundId()) || empty($notification->getOutboundShippingNotificationId())) {
            throw new InvalidArgumentException('Missing primary keys');
        }
        
        try {
            $response = $this->getClient()->getHttp()->request(
                'PATCH',
                sprintf('merchant/outbounds/%s/shipping-notifications/%s', $notification->getOutboundId(), $notification->getOutboundShippingNotificationId())
            );
            
            $result = $response->getStatusCode() === 204;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($notification->getOutboundId()));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
            
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationsByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/outbounds/shipping-notifications/updates',
            OutboundShippingNotification::class,
            $query,
            $this->buildCacheKey($query, 'merchant-osn-tf-')
        );
    }
}
