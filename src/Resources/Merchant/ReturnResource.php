<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnChangeShell;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\MerchantReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnItem as MerchantReturnItem;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Throwable;

/**
 * Class ReturnResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class ReturnResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/returns', MerchantReturn::class, $query, $this->buildCacheKey((string) $query));
    }

    /**
     * @param string $id
     * @param Query|null $query
     * @return MerchantReturn|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('merchant/returns/%s', $id), MerchantReturn::class, $this->buildCacheKey($id), $query);
    }

    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findUpdatesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/returns/updates',
            MerchantReturn::class,
            $query,
            $this->buildCacheKey('findUpdatesByTimeFrame' . $query)
        );
    }

    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/returns/changes',
            ReturnChangeShell::class,
            $query,
            $this->buildCacheKey('findChangesByTimeFrame' . $query)
        );
    }

    /**
     * @param string $id
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findChangesForReturnByTimeFrame(string $id, Query $query): TimeFrame
    {
        return $this->findUpdates(
            sprintf('merchant/returns/{%s}/changes', $id),
            ReturnChangeShell::class,
            $query,
            $this->buildCacheKey($id . $query)
        );
    }

    /**
     * @param MerchantReturn|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, MerchantReturn::class);

        /** @var MerchantReturn $model */
        return empty($model->getReturnId()) ?
            $this->create($model, 'merchant/returns', MerchantReturn::class, $model->property('returnId')) :
            $this->update(
                $model,
                sprintf('merchant/returns/%s', $model->getReturnId()),
                $this->buildCacheKey($model->getReturnId())
            );
    }

    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, MerchantReturn::class);

        /** @var MerchantReturn $model */
        return $this->delete(
            sprintf('merchant/returns/%s', $model->getReturnId()),
            $this->buildCacheKey($model->getReturnId())
        );
    }

    /**
     * @param string $returnId
     * @param MerchantReturnItem $item
     * @return MerchantReturnItem|null
     * @throws Throwable
     */
    public function addItem(string $returnId, MerchantReturnItem $item): ?MerchantReturnItem
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('merchant/returns/%s/items', $returnId),
                [
                    'body' => json_encode([
                        'data' => self::removeEmpty($item, $item->toArray())
                    ])
                ]
            );

            $this->getResourceCache()->delete($this->buildCacheKey($returnId));

            // Delete Page Cache
            $this->deletePageCache();

            return new MerchantReturnItem($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @param float $quantity
     * @param string|null $fulfillerReturnItemId
     * @return MerchantReturnItem|null
     * @throws Throwable
     */
    public function splitItem(string $returnId, string $itemId, float $quantity, ?string $fulfillerReturnItemId = null): ?MerchantReturnItem
    {
        try {
            $body = [
                'quantity' => $quantity
            ];

            if ($fulfillerReturnItemId !== null) {
                $body['returnItemId'] = $fulfillerReturnItemId;
            }

            $response = $this->getClient()->getHttp()->request(
                'POST',
                sprintf('merchant/returns/%s/items/%s', $returnId, $itemId),
                [
                    'body' => json_encode($body)
                ]
            );

            $this->getResourceCache()->delete($this->buildCacheKey($returnId));

            // Delete Page Cache
            $this->deletePageCache();

            return new MerchantReturnItem($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @return MerchantReturnItem|Model|null
     * @throws Throwable
     */
    public function findItem(string $returnId, string $itemId): ?Model
    {
        return $this->findBy(
            sprintf('merchant/returns/%s/items/%s', $returnId, $itemId),
            MerchantReturnItem::class,
            $this->buildCacheKey($returnId . '_' . $itemId)
        );
    }

    /**
     * @param string $returnId
     * @param MerchantReturnItem $item
     * @return bool
     * @throws Throwable
     */
    public function updateItem(string $returnId, MerchantReturnItem $item): bool
    {
        return $this->update(
            $item,
            sprintf('merchant/returns/%s/items/%s', $returnId, $item->getReturnItemId()),
            $this->buildCacheKey($item->getReturnItemId())
        );
    }

    /**
     * @param string $returnId
     * @param string $itemId
     * @return bool
     * @throws Throwable
     */
    public function removeItem(string $returnId, string $itemId): bool
    {
        return $this->delete(
            sprintf('merchant/returns/%s/items/%s', $returnId, $itemId),
            $this->buildCacheKey($itemId)
        );
    }

    /**
     * @param string $returnId
     * @return bool
     * @throws Throwable
     */
    public function lock(string $returnId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('merchant/returns/%s/lock', $returnId)
            );

            return $response->getStatusCode() === 204;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return false;
    }

    /**
     * @param string $returnId
     * @return bool
     * @throws Throwable
     */
    public function unlock(string $returnId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('merchant/returns/%s/unlock', $returnId)
            );

            return $response->getStatusCode() === 204;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return false;
    }
}
