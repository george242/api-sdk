<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon\SfpCredential;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;

/**
 * Class CredentialResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class CredentialResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function allAmazon(Query $query): Pagination
    {
        return $this->findAll(
            'merchant/credentials/amazonSfp',
            SfpCredential::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }

    /**
     * @param string $credentialId
     * @return SfpCredential|null|Model
     * @throws Throwable
     */
    public function findAmazon(string $credentialId): ?SfpCredential
    {
        return $this->findBy(
            sprintf('merchant/credentials/amazonSfp/%s', $credentialId),
            SfpCredential::class,
            $this->buildCacheKey($credentialId)
        );
    }
    
    /**
     * @param SfpCredential $credential
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function createAmazon(SfpCredential $credential): bool
    {
        return $this->create($credential, 'merchant/credentials/amazonSfp');
    }
    
    /**
     * @param SfpCredential $credential
     * @return bool
     * @throws Throwable
     */
    public function removeAmazon(SfpCredential $credential): bool
    {
        if ($credential === null) {
            return false;
        }
        
        return $this->delete(
            sprintf('merchant/credentials/amazonSfp/%s', $credential->getCredentialId()),
            $this->buildCacheKey($credential->getCredentialId())
        );
    }
}
