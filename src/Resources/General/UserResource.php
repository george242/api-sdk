<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\General;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\User;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class UserResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\General
 */
class UserResource extends Resource
{
    /**
     * @return User|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function current(): ?Model
    {
        // Can't be cached, cause of undefined state (current can be anyone)
        return $this->findBy('users/current', User::class);
    }
}
