<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\General;

use Exception;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Access\Token;
use Throwable;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\Access;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class AccessResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\General
 */
class AccessResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('access/locks', Access::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @return bool
     * @throws Throwable
     */
    public function lock(): bool
    {
        $response = $this->getClient()->getHttp()->request('POST', 'access/app/writeLock');
        
        return $response->getStatusCode() === 201;
    }
    
    /**
     * @return bool
     * @throws Throwable
     */
    public function unlock(): bool
    {
        $response = $this->getClient()->getHttp()->request('DELETE', 'access/app/writeLock');
    
        return $response->getStatusCode() === 204;
    }

    /**
     * @param Token $token
     * @return Token|null
     * @throws Throwable
     */
    public function createToken(Token $token): ?Token
    {
        try {
            $response = $this->getClient()->getHttp()->request('POST', 'access/tokens', [
                'body' => json_encode(self::removeEmpty($token, $token->toArray())),
            ]);

            if ($response->getStatusCode() !== 201) {
                return null;
            }

            return new Token($this->extractData($response));
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }

        return null;
    }
    
    /**
     * @param Query $query
     * @return Pagination
     * @throws JsonException
     */
    public function allTokens(Query $query): Pagination
    {
        return $this->findAll('access/tokens', Token::class, $query, $this->buildCacheKey(__METHOD__));
    }
    
    /**
     * @param string $tokenId
     * @return Token|Model|null
     * @throws Throwable
     */
    public function findToken(string $tokenId): ?Token
    {
        return $this->findBy(
            sprintf('access/tokens/%s', $tokenId),
            Token::class,
            $this->buildCacheKey($tokenId)
        );
    }
    
    /**
     * @param Token $token
     * @return bool
     * @throws Exception
     */
    public function removeToken(Token $token): bool
    {
        if ($token === null || empty($token->getTokenId())) {
            return false;
        }
    
        return $this->delete(
            sprintf('access/tokens/%s', $token->getTokenId()),
            $this->buildCacheKey($token->getTokenId())
        );
    }
}
