<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources;

use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Psr\SimpleCache\CacheInterface;

/**
 * Interface ResourceInterface
 * @package Jtl\Fulfillment\Api\Sdk\Resources
 */
interface ResourceInterface
{
    /**
     * @return Client
     */
    public function getClient(): Client;

    /**
     * @param Query $query
     * @return Pagination
     */
    public function all(Query $query): Pagination;

    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     */
    public function find(string $id, Query $query = null): ?Model;

    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     */
    public function save(Model $model, Query $query = null): bool;

    /**
     * @param Model
     * @param Query|null $query
     * @return bool
     */
    public function remove(Model $model, Query $query = null): bool;
}
