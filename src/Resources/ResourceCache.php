<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources;

use DateInterval;
use Exception;
use Log;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Serializable;

/**
 * Class ResourceCache
 * @package Jtl\Fulfillment\Api\Sdk\Resources
 */
class ResourceCache
{
    /**
     * @var int - Cache TTL in Minutes
     */
    protected $ttl;
    
    /**
     * @var CacheInterface
     */
    protected $cache;
    
    /**
     * ResourceCache constructor.
     * @param CacheInterface $cache
     * @param int - Cache TTL in Minutes
     */
    public function __construct(CacheInterface $cache, $ttl = null)
    {
        $this->cache = $cache;
        $this->ttl = $ttl ?: 30;
    }
    
    /**
     * @return int - Cache TTL in Minutes
     */
    public function getTtl(): int
    {
        return $this->ttl;
    }
    
    /**
     * @param int $ttl - Cache TTL in Minutes
     * @return self
     */
    public function setTtl(int $ttl): self
    {
        $this->ttl = $ttl;
        
        return $this;
    }
    
    /**
     * @return CacheInterface
     */
    public function getCache(): CacheInterface
    {
        return $this->cache;
    }
    
    /**
     * @param CacheInterface $cache
     * @return ResourceCache
     */
    public function setCache(CacheInterface $cache): ResourceCache
    {
        $this->cache = $cache;
        
        return $this;
    }
    
    /**
     * @param Serializable $value
     * @param string|null $cacheKey
     * @return bool
     */
    public function set(Serializable $value, string $cacheKey = null): bool
    {
        if ($cacheKey === null) {
            return true;
        }
    
        try {
            if ($this->cache->set($cacheKey, $value, new DateInterval(sprintf('PT%sM', $this->ttl)))) {
                return true;
            }
        
            return false;
        } catch (InvalidArgumentException $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        } catch (Exception $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        }
    }
    
    /**
     * @param array $values - A list of key => value pairs for a multiple-set operation.
     * @return bool
     */
    public function setMultiple(array $values): bool
    {
        try {
            return $this->cache->setMultiple($values);
        } catch (InvalidArgumentException $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        }
    }
    
    /**
     * @param string|null $cacheKey
     * @return mixed|null
     */
    public function get(string $cacheKey = null)
    {
        if ($cacheKey === null) {
            return null;
        }
        
        try {
            if ($this->cache->has($cacheKey)) {
                return $this->cache->get($cacheKey);
            }
    
            return null;
        } catch (InvalidArgumentException $e) {
            Log::error($e->getMessage(), exception_context($e));
            return null;
        }
    }
    
    /**
     * @param string|null $cacheKey
     * @return bool
     */
    public function delete(string $cacheKey = null): bool
    {
        if ($cacheKey === null) {
            return true;
        }
    
        try {
            return $this->cache->delete($cacheKey);
        } catch (InvalidArgumentException $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        }
    }
    
    /**
     * @param array $cacheKeys
     * @return bool
     */
    public function deleteMultiple(array $cacheKeys): bool
    {
        try {
            return $this->cache->deleteMultiple($cacheKeys);
        } catch (InvalidArgumentException $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        }
    }
    
    /**
     * @return bool
     */
    public function clear(): bool
    {
        try {
            return $this->cache->clear();
        } catch (Exception $e) {
            Log::error($e->getMessage(), exception_context($e));
            return false;
        }
    }
}
