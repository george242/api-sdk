<?php
namespace Jtl\Fulfillment\Api\Sdk;

use Exception;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface HttpInterface
 * @package Jtl\Fulfillment\Api\Sdk
 */
interface HttpInterface
{
    /**
     * Create and send an HTTP request.
     *
     * Use an absolute path to override the base path of the client, or a
     * relative path to append to the base path of the client. The URL can
     * contain the query string as well.
     *
     * @param string $method HTTP method.
     * @param string $uri URI object or string.
     * @param array $options Request options to apply.
     *
     * @return ResponseInterface
     * @throws Exception
     */
    public function request(string $method, string $uri, array $options = []): ResponseInterface;
}
