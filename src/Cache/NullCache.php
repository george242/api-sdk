<?php
namespace Jtl\Fulfillment\Api\Sdk\Cache;

use Psr\SimpleCache\CacheInterface;

/**
 * Class NullCache
 * @package Jtl\Fulfillment\Api\Sdk\Cache
 */
class NullCache implements CacheInterface
{
    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        return $default;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMultiple($keys, $default = null)
    {
        foreach ($keys as $key) {
            yield $key => $default;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function deleteMultiple($keys)
    {
        return true;
    }
    
    /**
     * {@inheritdoc}
     */
    public function set($key, $value, $ttl = null)
    {
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setMultiple($values, $ttl = null)
    {
        return false;
    }
}
