<?php
namespace Jtl\Fulfillment\Api\Sdk\Exceptions;

use Exception;

/**
 * Class MalformedDataException
 * @package Jtl\Fulfillment\Api\Sdk\Exceptions
 */
class MalformedDataException extends Exception
{
}
