<?php
namespace Jtl\Fulfillment\Api\Sdk\Exceptions;

use Exception;
use Throwable;

/**
 * Class HttpException
 * @package Jtl\Fulfillment\Api\Sdk\Exceptions
 */
class HttpException extends Exception
{
    /**
     * @param Throwable $e
     * @return bool
     */
    public static function isGuzzleException(Throwable $e): bool
    {
        return is_a($e, '\\GuzzleHttp\\Exception\\RequestException');
    }
    
    /**
     * @param Throwable $e
     * @throws Throwable
     */
    public static function handleGuzzeException(Throwable $e)
    {
        if (self::isGuzzleException($e) && $e->getResponse() && $e->getResponse()->getStatusCode() !== 404) {
            throw $e;
        }
    }
}
