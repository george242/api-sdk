<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItem;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemShell;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemStock;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemStockShell;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChangeIdChange;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnChange;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnItem as MerchantReturnItem;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChange;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChangeId;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\ReturnChangeShell;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\MerchantReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\ReturnResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class ReturnTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class ReturnTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/return_all.json', ReturnResource::class, MerchantReturn::class);
    }

    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            MerchantReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            MerchantReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            MerchantReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            MerchantReturn::class
        );
    }

    public function testCanQueryAllChanges(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_changes_all.json'));
        $timeFrame = (new ReturnResource($client))->findChangesByTimeFrame(new Query());

        $item = $timeFrame->getData()[0];
        $this->assertInstanceOf(ReturnChangeShell::class, $item);
        $this->assertEquals('FoooFlipp2134', $item->getId());

        $returnChange = $item->getChanges();
        $this->assertInstanceOf(ReturnChange::class, $returnChange);
        $this->assertEquals('FoooFlipp21123', $returnChange->getReturnId()->getValue());

        $this->assertCount(1, $returnChange->getItems());

        $returnItemShell = $returnChange->getItems()[0];
        $this->assertInstanceOf(ReturnChangeItemShell::class, $returnItemShell);
        $this->assertEquals('FoooFlipp123523', $returnItemShell->getId());

        $returnItem = $returnItemShell->getChanges();
        $this->assertInstanceOf(ReturnChangeItem::class, $returnItem);
        $this->assertEquals('string', $returnItem->getReturnItemId()->getValue());

        $this->assertCount(1, $returnItem->getStockChanges());

        $returnStockChange = $returnItem->getStockChanges()[0];
        $this->assertInstanceOf(ReturnChangeItemStockShell::class, $returnStockChange);
        $this->assertInstanceOf(ReturnStockChangeId::class, $returnStockChange->getId());
        $this->assertEquals('FULF04XX-12345-0001', $returnStockChange->getId()->getWarehouseId());

        $this->assertInstanceOf(ReturnChangeItemStock::class, $returnStockChange->getChanges());
        $this->assertInstanceOf(ReturnStockChangeIdChange::class, $returnStockChange->getChanges()->getStockChangeId());
        $this->assertEquals('string', $returnStockChange->getChanges()->getMerchantSku()->getValue());
    }

    public function testCanQueryChangesForReturn(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_changes_one.json'));
        $timeFrame = (new ReturnResource($client))->findChangesForReturnByTimeFrame('IUGuz324', new Query());

        $item = $timeFrame->getData()[0];

        $this->assertInstanceOf(ReturnChangeShell::class, $item);
        $this->assertEquals('2T8ew0g', $item->getId());

        $this->assertInstanceOf(ReturnChange::class, $item->getChanges());

        /** @var ReturnChange $returnChange */
        $returnChange = $item->getChanges();
        $this->assertEquals('IUGuz324', $returnChange->getReturnId()->getValue());
        $this->assertEquals('UIOGHIUgewv234', $returnChange->getFulfillerId()->getValue());

        $this->assertCount(1, $returnChange->getItems());

        $returnItemShell = $returnChange->getItems()[0];
        $this->assertInstanceOf(ReturnChangeItemShell::class, $returnItemShell);
        $this->assertEquals('FoooFlipp123523', $returnItemShell->getId());

        $returnItem = $returnItemShell->getChanges();
        $this->assertInstanceOf(ReturnChangeItem::class, $returnItem);
        $this->assertEquals('string', $returnItem->getReturnItemId()->getValue());

        $this->assertCount(1, $returnItem->getStockChanges());

        $returnStockChange = $returnItem->getStockChanges()[0];
        $this->assertInstanceOf(ReturnChangeItemStockShell::class, $returnStockChange);
        $this->assertInstanceOf(ReturnStockChangeId::class, $returnStockChange->getId());
        $this->assertEquals('FULF04XX-12345-0001', $returnStockChange->getId()->getWarehouseId());

        $this->assertInstanceOf(ReturnChangeItemStock::class, $returnStockChange->getChanges());
        $this->assertInstanceOf(ReturnStockChangeIdChange::class, $returnStockChange->getChanges()->getStockChangeId());
        $this->assertEquals('string', $returnStockChange->getChanges()->getMerchantSku()->getValue());
    }

    public function testCanQueryAllUpdates(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/return_updates_all.json',
            ReturnResource::class,
            MerchantReturn::class,
            'findUpdatesByTimeFrame',
            function (TimeFrame $timeFrame) {
                /** @var MerchantReturn $return */
                $return = $timeFrame->getData()[0];
                $this->assertInstanceOf(MerchantReturn::class, $return);
                $this->assertEquals('MERC05SFERGF', $return->getReturnId());

                $this->assertCount(1, $return->getItems());

                $item = $return->getItems()[0];

                $this->assertInstanceOf(MerchantReturnItem::class, $item);
                $this->assertEquals('MERC05PosFDSE', $item->getReturnItemId());

                $this->assertCount(1, $item->getStockChanges());

                /** @var ReturnStockChange $stockChange */
                $stockChange = $item->getStockChanges()[0];
                $this->assertInstanceOf(ReturnStockChange::class, $stockChange);
                $this->assertInstanceOf(ReturnStockChangeId::class, $stockChange->getStockChangeId());
                $this->assertEquals('MERC01PRDCT', $stockChange->getStockChangeId()->getJfsku());
                $this->assertEquals('ART2394871', $stockChange->getMerchantSku());
            }
        );
    }

    public function testCanFindOneItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $merchantReturnItem = (new ReturnResource($client))->findItem('IUGuz324', 'MERC05PosFDSE');

        $this->assertInstanceOf(MerchantReturnItem::class, $merchantReturnItem);
        $this->assertEquals('MERC05PosFDSE', $merchantReturnItem->getReturnItemId());
    }

    public function testCanUpdateItem(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->updateItem('IUGuz324', new MerchantReturnItem(
            json_decode(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), true)
        ));

        $this->assertTrue($result);
    }

    public function testCanCreateItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $merchantReturnItem = (new ReturnResource($client))->addItem('IUGuz324', new MerchantReturnItem(
            json_decode(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), true)
        ));

        $this->assertInstanceOf(MerchantReturnItem::class, $merchantReturnItem);
        $this->assertEquals('MERC05PosFDSE', $merchantReturnItem->getReturnItemId());
    }

    public function testCanRemoveItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), 204);
        $result = (new ReturnResource($client))->removeItem('IUGuz324', 'MERC05PosFDSE');

        $this->assertTrue($result);
    }

    public function testCanSplitItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $merchantReturnItem = (new ReturnResource($client))->splitItem('IUGuz324', 'MERC05PosFDSE', 203.202);

        $this->assertInstanceOf(MerchantReturnItem::class, $merchantReturnItem);
        $this->assertEquals('MERC05PosFDSE', $merchantReturnItem->getReturnItemId());
    }

    public function testCanLockAReturn(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->lock('IUGuz324');

        $this->assertTrue($result);
    }

    public function testCanUnLockAReturn(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->unlock('IUGuz324');

        $this->assertTrue($result);
    }
}
