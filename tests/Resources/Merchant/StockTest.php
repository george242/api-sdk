<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevel;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevelWarehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock\StockComplete;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\StockResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class StockTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class StockTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/stock_all.json', StockResource::class, ProductStockLevel::class);
    }
    
    public function testCanFindOneBySku(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/stock_one.json',
            StockResource::class,
            ProductStockLevel::class,
            'findBySku',
            'MERC01PRDCT',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanFindOneBySkuAndWarehouse(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/stock_warehouse_one.json')),
            new Response(403),
            new Response(404),
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new StockResource($client);
        
        $model = $resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001');
        
        $this->assertInstanceOf(ProductStockLevelWarehouse::class, $model);
        $this->assertEquals('MERC01PRDCT', $model->getJfsku());
    
        $this->expectException(ClientException::class);
        $resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001');
    
        $this->assertEmpty($resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001'));
    }
    
    public function testCanQueryAllUpdates(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/stock_update_all.json',
            StockResource::class,
            StockChange::class,
            'allUpdates'
        );
    }
    
    public function testCanQueryAllChanges(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/stock_change_all.json',
            StockResource::class,
            StockChange::class,
            'allChanges'
        );
    }

    public function testCanQueryAllCompletes(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/stock_complete_all.json',
            StockResource::class,
            StockComplete::class,
            'allCompletes'
        );
    }
}
