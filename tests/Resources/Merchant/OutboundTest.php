<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\Model;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Attachment;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\OutboundAmazonSfpShippingLabelProviderData;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\OutboundShippingLabelProviderData;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\OutboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\Outbound;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Returns\MerchantReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\OutboundResource;
use Jtl\Fulfillment\Api\Sdk\Resources\ResourceInterface;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class OutboundTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class OutboundTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/outbound_all.json',
            OutboundResource::class,
            Outbound::class,
            'all',
            function (Pagination $pagination) {
                /** @var Outbound $outbound */
                $outbound = $pagination->getItems()[0];
                $this->assertEquals('MERC02AXEV', $outbound->getOutboundId());
                $this->assertCount(1, $outbound->getRelatedReturns());

                $relatedReturn = $outbound->getRelatedReturns()[0];
                $this->assertInstanceOf(MerchantReturn::class, $relatedReturn);
                $this->assertEquals('FCTH05E', $relatedReturn->getReturnId());
                
                // FFN-701
                $shippingLabelProviderData = $outbound->getShippingLabelProviderData();
                $this->assertInstanceOf(OutboundShippingLabelProviderData::class, $shippingLabelProviderData);
                $amazonSfpShippingLabelProviderData = $shippingLabelProviderData->getAmazonSfpShippingLabelProviderData();
                $this->assertInstanceOf(OutboundAmazonSfpShippingLabelProviderData::class, $amazonSfpShippingLabelProviderData);
                $this->assertEquals('S12345659XY', $amazonSfpShippingLabelProviderData->getSellerId());
            }
        );
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/outbound_one.json',
            OutboundResource::class,
            Outbound::class,
            'MERC02AXEV',
            new PropertyInfo('outboundId')
        );
    }
    
    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/outbound_one.json',
            OutboundResource::class,
            Outbound::class,
            'MERC02AXEV',
            new PropertyInfo('outboundId')
        );
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/outbound_one.json',
            OutboundResource::class,
            Outbound::class,
            'MERC02AXEV',
            new PropertyInfo('outboundId')
        );
    }
    
    public function testCanChangeStatus(): void
    {
        $mock = new MockHandler([
            new Response(204),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $this->assertTrue($resource->changeStatus('MERC02AXEV', 'Preparation'));
    
        $this->expectException(ClientException::class);
        $resource->changeStatus('MERC02AXEV', 'Preparation');
    
        $this->assertFalse($resource->changeStatus('MERC02AXEV', 'Preparation'));
    }
    
    public function testCanCreateAnAttachment(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/attachment_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        /** @var Attachment $model */
        $model = $resource->addAttachment('MERC02AXEV', 'Foobar', 'Invoice', '123');
    
        $this->assertInstanceOf(Attachment::class, $model);
        $this->assertEquals('Foobar', $model->getMerchantDocumentId());
    
        $this->expectException(ClientException::class);
        $resource->addAttachment('MERC02AXEV', 'Foobar', 'Invoice', '123');
    
        $this->assertEmpty($resource->addAttachment('MERC02AXEV', 'Foobar', 'Invoice', '123'));
    }
    
    public function testAttachmentCanBeRemoved(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $this->assertTrue($resource->removeAttachment('MERC02AXEV', 'Foobar'));
    
        $this->expectException(ClientException::class);
        $resource->removeAttachment('MERC02AXEV', 'Foobar');
    
        $this->assertFalse($resource->removeAttachment('MERC02AXEV', 'Foobar'));
    }
    
    public function testCanFindAnAttachment(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/attachment_one.json'));
    
        /** @var OutboundResource $resource */
        $resource = new OutboundResource($client);
    
        /** @var Attachment $model */
        $model = $resource->findAttachment('MERC02AXEV', 'Foobar');
    
        $this->assertInstanceOf(Attachment::class, $model);
        $this->assertEquals('Foobar', $model->getMerchantDocumentId());
    
        $this->expectException(ClientException::class);
        $resource->findAttachment('MERC02AXEV', 'Foobar');
    
        $this->assertEmpty($resource->findAttachment('MERC02AXEV', 'Foobar'));
    }
    
    public function testCanQueryAllChanges(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/outbound_changes_all.json',
            OutboundResource::class,
            Outbound::class,
            'findChangesByTimeFrame'
        );
    }
    
    public function testFindShippingNotifications(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_notification_all.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $pagination = $resource->findShippingNotifications('MERC02AXEV');
    
        $this->assertInstanceOf(Pagination::class, $pagination);
        $this->assertEquals(1, $pagination->getPage()->getTotal());
        $this->assertInstanceOf(OutboundShippingNotification::class, $pagination->getItems()[0]);
    
        $this->expectException(ClientException::class);
        $resource->findShippingNotifications('MERC02AXEV');
    
        $this->assertEmpty($resource->findShippingNotifications('MERC02AXEV'));
    }
    
    public function testCanFindShippingNotificationById(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_notification_one.json')),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new OutboundResource($client);
        
        $notification = $resource->findShippingNotificationById('MERC02AXEV', 'Foobar');
        
        $this->assertInstanceOf(OutboundShippingNotification::class, $notification);
        
        $this->expectException(ClientException::class);
        $resource->findShippingNotificationById('MERC02AXEV', 'Foobar');
        
        $this->assertEmpty($resource->findShippingNotificationById('MERC02AXEV', 'Foobar'));
    }
    
    public function testCanUpdateShippingNotification(): void
    {
        $json = file_get_contents(__DIR__ . '/MockData/outbound_notification_one.json');
        $client = $this->prepareHttpHandler(null, 204);
    
        $resource = new OutboundResource($client);
    
        $model = new OutboundShippingNotification(json_decode($json, true));
        $result = $resource->updateShippingNotification($model);
    
        $this->assertTrue($result);
        $this->assertEquals('MERC06OUTBNDSHPNTFCN01', $model->getOutboundShippingNotificationId());
    
        $this->expectException(ClientException::class);
        $resource->updateShippingNotification($model);
    
        $this->expectException(ClientException::class);
        $resource->updateShippingNotification($model);
    }
    
    public function testCanFindShippingNotificationsByTimeFrame(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/outbound_notification_timeframe_all.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new OutboundResource($client);
    
        $notification = $resource->findShippingNotificationsByTimeFrame(new Query());
    
        $this->assertInstanceOf(TimeFrame::class, $notification);
        $this->assertInstanceOf(OutboundShippingNotification::class, $notification->getData()[0]);
    
        $this->expectException(ClientException::class);
        $resource->findShippingNotificationsByTimeFrame(new Query());
    
        $this->assertEmpty($resource->findShippingNotificationsByTimeFrame(new Query()));
    }
}
