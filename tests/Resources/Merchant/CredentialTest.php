<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon\SfpCredential;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\CredentialResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client as HttpClient;

/**
 * Class CredentialTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class CredentialTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/credential_amazon_all.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new CredentialResource($client);
    
        /** @var Pagination $result */
        $result = $resource->allAmazon(new Query());
        $items = $result->getItems();
    
        $this->assertCount(1, $items);
        $this->assertInstanceOf(SfpCredential::class, $items[0]);
    
        $this->expectException(ClientException::class);
        $resource->allAmazon(new Query());
    
        $this->assertEmpty($resource->allAmazon(new Query()));
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/credential_amazon_one.json',
            CredentialResource::class,
            SfpCredential::class,
            'findAmazon',
            'ABC'
        );
    }
    
    public function testCanCreateOne(): void
    {
        $json = file_get_contents(__DIR__ . '/MockData/credential_amazon_one.json');
        $client = $this->prepareHttpHandler($json, 201);
    
        /** @var CredentialResource $resource */
        $resource = new CredentialResource($client);
    
        /** @var SfpCredential $model */
        $model = new SfpCredential(json_decode($json, true));
    
        $result = $resource->createAmazon($model);
    
        $this->assertTrue($result);
    
        $this->expectException(ClientException::class);
        $resource->createAmazon($model);
    
        $this->assertEmpty($resource->createAmazon($model));
    }
    
    public function testCanRemoveOne(): void
    {
        $json = file_get_contents(__DIR__ . '/MockData/credential_amazon_one.json');
        $client = $this->prepareHttpHandler(null, 204);
    
        /** @var CredentialResource $resource */
        $resource = new CredentialResource($client);
    
        /** @var SfpCredential $model */
        $model = new SfpCredential(json_decode($json, true));
    
        $this->assertTrue($resource->removeAmazon($model));
    
        $this->expectException(ClientException::class);
        $resource->removeAmazon($model);
    
        $this->assertFalse($resource->removeAmazon($model));
    }
}
