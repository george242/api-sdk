<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevel;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevelWarehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockAdjustment;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\StockResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class StockTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class StockTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/stock_all.json', StockResource::class, ProductStockLevel::class);
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/stock_one.json',
            StockResource::class,
            StockAdjustment::class
        );
    }
    
    public function testCanFindOneBySku(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/stock_one.json',
            StockResource::class,
            ProductStockLevel::class,
            'findBySku',
            'MERC01PRDCT',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanFindOneBySkuAndWarehouseId(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/stock_warehouse_one.json'));
    
        $resource = new StockResource($client);
    
        $model = $resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001');
    
        $this->assertInstanceOf(ProductStockLevelWarehouse::class, $model);
        $this->assertEquals('MERC01PRDCT', $model->getJfsku());
    
        $this->expectException(ClientException::class);
        $resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001');
    
        $this->assertEmpty($resource->findBySkuAndWarehouse('MERC01PRDCT', 'FULF04XX-12345-0001'));
    }
    
    public function testCanFindOneByWarehouseId(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/stock_warehouse_all.json'));
        
        $resource = new StockResource($client);
    
        $result = $resource->findByWarehouse('FULF04XX-12345-0001', new Query());
    
        $items = $result->getItems();
    
        $this->assertCount(1, $items);
        $this->assertInstanceOf(ProductStockLevelWarehouse::class, $items[0]);
    
        $this->expectException(ClientException::class);
        $resource->findByWarehouse('FULF04XX-12345-0001', new Query());
    
        $this->assertEmpty($resource->findByWarehouse('FULF04XX-12345-0001', new Query()));
    }
    
    public function testCanQueryAllChanges(): void
    {
        $this->canQueryAll(
            __DIR__ . '/MockData/stock_change_all.json',
            StockResource::class,
            StockChange::class,
            'allChanges'
        );
    }
    
    public function testCanQueryAllUpdates(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/stock_update_all.json',
            StockResource::class,
            StockChange::class,
            'allUpdates'
        );
    }
    
    public function testCanFindChangesByWarehouseId(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/stock_change_all.json'));
    
        $resource = new StockResource($client);
    
        $result = $resource->findChangesByWarehouse('FULF04XX-12345-0001', new Query());
    
        $items = $result->getItems();
    
        $this->assertCount(1, $items);
        $this->assertInstanceOf(StockChange::class, $items[0]);
    
        $this->expectException(ClientException::class);
        $resource->findChangesByWarehouse('FULF04XX-12345-0001', new Query());
    
        $this->assertEmpty($resource->findChangesByWarehouse('FULF04XX-12345-0001', new Query()));
    }
}
