<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\FulfillerReturn;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnChange;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnChangeShell;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnIncomingItem;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock\StockChange;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItem;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemShell;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemStock;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnChangeItemStockShell;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChange;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChangeId;
use Jtl\Fulfillment\Api\Sdk\Models\General\Returns\ReturnStockChangeIdChange;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\ReturnResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Returns\ReturnItem as FulfillerReturnItem;

/**
 * Class ReturnTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class ReturnTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/return_all.json', ReturnResource::class, FulfillerReturn::class);
    }

    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            FulfillerReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            FulfillerReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            FulfillerReturn::class,
            'MERC05SFERGF',
            new PropertyInfo('returnId')
        );
    }

    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/return_one.json',
            ReturnResource::class,
            FulfillerReturn::class
        );
    }

    public function testCanQueryAllChanges(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_changes_all.json'));
        $timeFrame = (new ReturnResource($client))->findChangesByTimeFrame(new Query());

        $item = $timeFrame->getData()[0];
        $this->assertInstanceOf(ReturnChangeShell::class, $item);
        $this->assertEquals('FoooFlipp2134', $item->getId());

        $returnChange = $item->getChanges();
        $this->assertInstanceOf(ReturnChange::class, $returnChange);
        $this->assertEquals('FoooFlipp21123', $returnChange->getReturnId()->getValue());

        $this->assertCount(1, $returnChange->getItems());

        $returnItemShell = $returnChange->getItems()[0];
        $this->assertInstanceOf(ReturnChangeItemShell::class, $returnItemShell);
        $this->assertEquals('FoooFlipp123523', $returnItemShell->getId());


        $returnItem = $returnItemShell->getChanges();
        $this->assertInstanceOf(ReturnChangeItem::class, $returnItem);
        $this->assertEquals('string', $returnItem->getReturnItemId()->getValue());

        $this->assertCount(1, $returnItem->getStockChanges());

        $returnStockChange = $returnItem->getStockChanges()[0];
        $this->assertInstanceOf(ReturnChangeItemStockShell::class, $returnStockChange);
        $this->assertInstanceOf(ReturnStockChangeId::class, $returnStockChange->getId());
        $this->assertEquals('FULF04XX-12345-0001', $returnStockChange->getId()->getWarehouseId());

        $this->assertInstanceOf(ReturnChangeItemStock::class, $returnStockChange->getChanges());
        $this->assertInstanceOf(ReturnStockChangeIdChange::class, $returnStockChange->getChanges()->getStockChangeId());
        $this->assertEquals('string', $returnStockChange->getChanges()->getMerchantSku()->getValue());
    }

    public function testCanQueryChangesForReturn(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_changes_one.json'));
        $timeFrame = (new ReturnResource($client))->findChangesForReturnByTimeFrame('IUGuz324', new Query());

        $item = $timeFrame->getData()[0];

        $this->assertInstanceOf(ReturnChangeShell::class, $item);
        $this->assertEquals('2T8ew0g', $item->getId());

        $this->assertInstanceOf(ReturnChange::class, $item->getChanges());

        /** @var ReturnChange $returnChange */
        $returnChange = $item->getChanges();
        $this->assertEquals('IUGuz324', $returnChange->getReturnId()->getValue());
        $this->assertEquals('234IUGWEGB', $returnChange->getMerchantId()->getValue());

        $this->assertCount(1, $returnChange->getItems());

        $returnItemShell = $returnChange->getItems()[0];
        $this->assertInstanceOf(ReturnChangeItemShell::class, $returnItemShell);
        $this->assertEquals('FoooFlipp123523', $returnItemShell->getId());

        $returnItem = $returnItemShell->getChanges();
        $this->assertInstanceOf(ReturnChangeItem::class, $returnItem);
        $this->assertEquals('string', $returnItem->getReturnItemId()->getValue());

        $this->assertCount(1, $returnItem->getStockChanges());

        $returnStockChange = $returnItem->getStockChanges()[0];
        $this->assertInstanceOf(ReturnChangeItemStockShell::class, $returnStockChange);
        $this->assertInstanceOf(ReturnStockChangeId::class, $returnStockChange->getId());
        $this->assertEquals('FULF04XX-12345-0001', $returnStockChange->getId()->getWarehouseId());

        $this->assertInstanceOf(ReturnChangeItemStock::class, $returnStockChange->getChanges());
        $this->assertInstanceOf(ReturnStockChangeIdChange::class, $returnStockChange->getChanges()->getStockChangeId());
        $this->assertEquals('string', $returnStockChange->getChanges()->getMerchantSku()->getValue());
    }

    public function testCanQueryAllUpdates(): void
    {
        $this->canQueryUpdates(
            __DIR__ . '/MockData/return_updates_all.json',
            ReturnResource::class,
            FulfillerReturn::class,
            'findUpdatesByTimeFrame',
            function (TimeFrame  $timeFrame) {
                /** @var FulfillerReturn $return */
                $return = $timeFrame->getData()[0];
                $this->assertInstanceOf(FulfillerReturn::class, $return);
                $this->assertEquals('MERC05SFERGF', $return->getReturnId());

                $this->assertCount(1, $return->getItems());

                $item = $return->getItems()[0];

                $this->assertInstanceOf(FulfillerReturnItem::class, $item);
                $this->assertEquals('MERC05PosFDSE', $item->getReturnItemId());

                $this->assertCount(1, $item->getStockChanges());

                /** @var ReturnStockChange $stockChange */
                $stockChange = $item->getStockChanges()[0];
                $this->assertInstanceOf(ReturnStockChange::class, $stockChange);
                $this->assertInstanceOf(ReturnStockChangeId::class, $stockChange->getStockChangeId());
                $this->assertEquals('MERC01PRDCT', $stockChange->getStockChangeId()->getJfsku());
                $this->assertEquals('ART2394871', $stockChange->getMerchantSku());
            }
        );
    }

    public function testCanFindOneItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $fulfillerReturnItem = (new ReturnResource($client))->findItem('IUGuz324', 'MERC05PosFDSE');

        $this->assertInstanceOf(FulfillerReturnItem::class, $fulfillerReturnItem);
        $this->assertEquals('MERC05PosFDSE', $fulfillerReturnItem->getReturnItemId());
    }

    public function testCanUpdateItem(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->updateItem('IUGuz324', new FulfillerReturnItem(
            json_decode(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), true)
        ));

        $this->assertTrue($result);
    }

    public function testCanCreateItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $fulfillerReturnItem = (new ReturnResource($client))->addItem('IUGuz324', new FulfillerReturnItem(
            json_decode(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), true)
        ));

        $this->assertInstanceOf(FulfillerReturnItem::class, $fulfillerReturnItem);
        $this->assertEquals('MERC05PosFDSE', $fulfillerReturnItem->getReturnItemId());
    }

    public function testCanRemoveItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'), 204);
        $result = (new ReturnResource($client))->removeItem('IUGuz324', 'MERC05PosFDSE');

        $this->assertTrue($result);
    }

    public function testCanSplitItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/return_item_one.json'));
        $fulfillerReturnItem = (new ReturnResource($client))->splitItem('IUGuz324', 'MERC05PosFDSE', 203.202);

        $this->assertInstanceOf(FulfillerReturnItem::class, $fulfillerReturnItem);
        $this->assertEquals('MERC05PosFDSE', $fulfillerReturnItem->getReturnItemId());
    }

    public function testCanPostIncomingItem(): void
    {
        $client = $this->prepareHttpHandler(file_get_contents(__DIR__ . '/MockData/stock_change_one.json'));
        $stockChange = (new ReturnResource($client))->incomingItem(
            'IUGuz324',
            'MERC05PosFDSE',
            new ReturnIncomingItem(
                json_decode(file_get_contents(__DIR__ . '/MockData/return_incoming_item_one.json'), true)
            )
        );

        $this->assertInstanceOf(StockChange::class, $stockChange);
        $this->assertEquals('BK23004', $stockChange->getFulfillerStockChangeId());
    }

    public function testCanLockAReturn(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->lock('IUGuz324');

        $this->assertTrue($result);
    }

    public function testCanUnLockAReturn(): void
    {
        $client = $this->prepareHttpHandler('', 204);
        $result = (new ReturnResource($client))->unlock('IUGuz324');

        $this->assertTrue($result);
    }
}
