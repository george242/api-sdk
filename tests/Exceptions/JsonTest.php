<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Exceptions;

use Exception;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use PHPUnit\Framework\TestCase;

/**
 * Class JsonTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Exceptions
 */
class JsonTest extends TestCase
{
    public function testJsonExceptionExtendsException(): void
    {
        $this->assertInstanceOf(Exception::class, new JsonException());
    }
}
